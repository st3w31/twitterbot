/** @type {import('jest').Config} */
const config = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.js',
    '!src/www/server.js',
    '!src/config/**/*.js',
  ],
  coverageDirectory: 'coverage',
  coverageProvider: 'babel',
  moduleNameMapper: {
    '@/(.*)': '<rootDir>/src/$1',
    '@mocks/(.*)': '<rootDir>/tests/__mocks__/$1',
  },
  modulePathIgnorePatterns: ['<rootDir>/dist/'],
};

module.exports = config;
