import { auth } from 'twitter-api-sdk';

const options = {
  client_id: process.env.TWITTER_CLIENT_ID,
  client_secret: process.env.TWITTER_CLIENT_SECRET,
  callback: process.env.TWITTER_CALLBACK_URL,
  scopes: [
    'tweet.read',
    'tweet.write',
    'users.read',
    'follows.write',
    'offline.access',
  ],
};

export const authClientFromToken = (token) => {
  return new auth.OAuth2User({ ...options, token });
};

export default new auth.OAuth2User(options);
