import { createClient } from 'redis';

const redisUrl = `redis://:${process.env.REDIS_PASS}@cache:6379`
const client = createClient({ legacyMode: true , url: redisUrl
});

(async () => {
  try {
    console.log("waiting for client")
    await client.connect();
    console.log("connected")
  } catch (error) {
    await client.disconnect();
  }
})();

export default client;
