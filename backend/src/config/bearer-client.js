import { Client } from 'twitter-api-sdk';

export default new Client(process.env.TWITTER_BEARER_TOKEN);
