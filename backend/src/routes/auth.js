import { randomBytes } from 'crypto';
import { matchedData } from 'express-validator';
import { InternalServerError } from 'http-errors';

import authClient, { authClientFromToken } from '@/config/auth-client.js';
import isAuth from '@/middlewares/is-auth.js';
import isGuest from '@/middlewares/is-guest.js';
import validationErrorHandler from '@/middlewares/validation-error-handler.js';
import authValidator from '@/validators/auth/auth.validator.js';

/**
 * Status.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const status = async (req, res) => {
  const isAuthenticated =
    req.session?.token &&
    !authClientFromToken(req.session.token).isAccessTokenExpired();

  res.status(200).json({ isAuthenticated });
};

/**
 * Login.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const login = async (req, res) => {
  // See:
  // - https://www.oauth.com/oauth2-servers/single-page-apps/
  // - https://fusebit.io/blog/oauth-state-parameters-nodejs/
  // "An OAuth 2.0 state parameter is a unique, randomly generated, opaque,
  // and non-guessable string that is sent when starting an authentication
  // request and validated when processing the response."
  const state = randomBytes(128).toString('hex');

  const authUrl = authClient.generateAuthURL({
    state,
    code_challenge_method: 's256',
  });

  req.session.state = state;
  res.redirect(authUrl);
};

/**
 * Callback.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const callback = async (req, res) => {
  const { code, state } = matchedData(req, {
    locations: ['query'],
    includeOptionals: false,
  });

  if (state !== req.session.state) {
    throw new InternalServerError("State isn't matching.");
  }

  const { token } = await authClient.requestAccessToken(code);
  req.session.token = token;
  delete req.session.state;

  res.redirect(process.env.CLIENT_BASE_URL);
};

/**
 * Logout.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const logout = async (req, res) => {
  const { revoked } = await authClientFromToken(
    req.session.token,
  ).revokeAccessToken();
  req.session.destroy();
  res.status(200).json({ revoked });
};

/**
 * Auth routes.
 *
 * @param {import('express').Router} router Router.
 * @returns {import('express').Router} Router.
 */
export default function auth(router) {
  router.get('/status', status);
  router.get('/login', isGuest, login);
  router.get(
    '/callback',
    isGuest,
    ...authValidator,
    validationErrorHandler,
    callback,
  );
  router.get('/logout', isAuth, logout);

  return router;
}
