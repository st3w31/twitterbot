import { body, matchedData } from 'express-validator';
import { generate } from 'randomstring';
import { Client } from 'twitter-api-sdk';

import { authClientFromToken } from '@/config/auth-client.js';
import isAuth from '@/middlewares/is-auth.js';
import validationErrorHandler from '@/middlewares/validation-error-handler.js';

/**
 * Create poll.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const chessPollCreate = async (req, res) => {
  const { moves } = matchedData(req, {
    locations: ['body'],
    includeOptionals: false,
  });
  const fenUrl = 'https://fen2png.com/api/?fen=' + req.body.fen + '&raw=true';
  const text =
    'Move ID:' +
    generate({ length: 16 }) +
    '\n' +
    moves +
    '\n' +
    encodeURI(fenUrl);

  const tweet = await new Client(
    authClientFromToken(req.session.token),
  ).tweets.createTweet({ text });

  res.status(200).json({ ...tweet.data });
};

/**
 * Get poll replies.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const chessPollReplies = async (req, res) => {
  const { pollId } = matchedData(req, {
    locations: ['body'],
    includeOptionals: false,
  });

  const { data: tweets } = await new Client(
    authClientFromToken(req.session.token),
  ).tweets.tweetsRecentSearch({
    query: `conversation_id:${pollId} is:reply`,
    'tweet.fields': 'text,public_metrics',
  });

  if (tweets) {
    tweets.sort(
      (a, b) => b.public_metrics.like_count > a.public_metrics.like_count,
    );

    for (let tweet of tweets) {
      tweet.text = tweet.text.split(' ')[1];
    }
  }

  res
    .status(200)
    .json({ move: tweets?.length > 0 ? tweets[0].text : undefined });
};

/**
 * Chess routes.
 *
 * @param {import('express').Router} router Router.
 * @returns {import('express').Router} Router.
 */
export default function chess(router) {
  router.post(
    '/chess/poll/create',
    isAuth,
    body('moves').isString(),
    validationErrorHandler,
    chessPollCreate,
  );

  router.post(
    '/chess/poll/replies',
    isAuth,
    body('pollId').isString(),
    validationErrorHandler,
    chessPollReplies,
  );

  return router;
}
