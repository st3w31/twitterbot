import { matchedData } from 'express-validator';

import getGhigliottinaWinners from '@/api/get-ghigliottina-winners';
import getTweetsByHashtag from '@/api/get-tweets-by-hashtag.js';
import getTweetsByLocation from '@/api/get-tweets-by-location.js';
import getUserTweets from '@/api/get-user-tweets.js';
import validationErrorHandler from '@/middlewares/validation-error-handler.js';
import dateFilterValidator from '@/validators/date-filter/date-filter.validator.js';
import hashtagValidator from '@/validators/hashtag/hashtag.validator.js';
import countryCodesValidator from '@/validators/location/country-codes.validator.js';
import maxResultsValidator from '@/validators/max-results/max-results.validator.js';
import usernameValidator from '@/validators/username/username.validator.js';

/**
 * Tweets by username.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const tweetsByUsername = async (req, res) => {
  const { username, startTime, endTime, maxResults } = matchedData(req, {
    locations: ['body'],
    includeOptionals: false,
  });

  const tweets = await getUserTweets(
    username,
    { startTime, endTime },
    maxResults,
  );

  res.status(200).json(tweets);
};

/**
 * Tweets by location.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const tweetsByLocation = async (req, res) => {
  const { countryCodes, startTime, endTime, maxResults } = matchedData(req, {
    locations: ['body'],
    includeOptionals: false,
  });

  const tweets = await getTweetsByLocation(
    { countryCodes },
    { startTime, endTime },
    maxResults,
  );

  res.status(200).json(tweets);
};

/**
 * Tweets by hashtag.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 */
export const tweetsByHashtag = async (req, res) => {
  const { keywords, startTime, endTime, maxResults } = matchedData(req, {
    locations: ['body'],
    includeOptionals: false,
  });

  const tweets = await getTweetsByHashtag(
    keywords,
    { startTime, endTime },
    maxResults,
  );

  res.status(200).json(tweets);
};

/**
 * Api routes.
 *
 * @param {import('express').Router} router Router.
 * @returns {import('express').Router} Router.
 */
export default function api(router) {
  router.post(
    '/tweets/by/username',
    ...usernameValidator,
    ...dateFilterValidator,
    ...maxResultsValidator,
    validationErrorHandler,
    tweetsByUsername,
  );

  router.post(
    '/tweets/by/location',
    ...countryCodesValidator,
    ...dateFilterValidator,
    ...maxResultsValidator,
    validationErrorHandler,
    tweetsByLocation,
  );

  router.post(
    '/tweets/by/hashtag',
    ...hashtagValidator,
    ...dateFilterValidator,
    ...maxResultsValidator,
    validationErrorHandler,
    tweetsByHashtag,
  );
  router.post(
    '/eredita',
    ...dateFilterValidator,
    validationErrorHandler,
    async (req, res, next) => {
      try {
        const { startTime, endTime } = matchedData(req, {
          locations: ['body'],
          includeOptionals: false,
        });
        const tweets = await getGhigliottinaWinners({ startTime, endTime });
        return res.status(200).json(tweets);
      } catch (error) {
        next(error);
      }
    },
  );

  return router;
}
