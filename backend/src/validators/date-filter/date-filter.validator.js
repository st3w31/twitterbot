import { body } from 'express-validator';
import { DateTime } from 'luxon';

/**
 * Check that the "startTime" and "endTime" fields exist.
 */
export const exists = [
  body(['startTime', 'endTime'])
    .exists({ checkFalsy: true })
    .withMessage(
      (_value, { path }) => `The "${path}" query parameter can not be empty.`,
    ),
];

/**
 * Check that the "startTime" and "endTime" fields are valid ISO8601 date-time.
 */
export const ISO8601 = [
  body(['startTime', 'endTime']).custom((value, { path }) => {
    const escapeLuxonFormat = (format, chars) => {
      chars.forEach((char) => {
        format = format.replaceAll(char, `'${char}'`);
      });

      return format;
    };

    const baseChars = ['-', 'T', ':', '.'];
    const allowedFormats = [
      escapeLuxonFormat('yyyy-MM-ddTHH:mm:ss.SSSZZ', baseChars),
      escapeLuxonFormat('yyyy-MM-ddTHH:mm:ss.SSSZ', [...baseChars, 'Z']),
    ];

    if (
      !DateTime.fromFormat(value, allowedFormats[0]).isValid &&
      !DateTime.fromFormat(value, allowedFormats[1]).isValid
    ) {
      throw new Error(
        `The "${path}" query parameter value [${value}] is not a valid ISO8601 date-time.`,
      );
    }

    return true;
  }),
];

/**
 * Check that the "startTime" and "endTime" fields are not future dates.
 */
export const beforeNow = [
  body(['startTime', 'endTime']).custom((value, { path }) => {
    if (new Date(value) > new Date()) {
      throw new Error(
        `The "${path}" query parameter value [${value}] can not be a date in the future.`,
      );
    }

    return true;
  }),
];

/**
 * Check that the "startTime" and "endTime" fields are not before March 21, 2006.
 */
export const after21March2006 = [
  body(['startTime', 'endTime']).custom((value, { path }) => {
    if (new Date(value) < new Date('2006-03-21')) {
      throw new Error(
        `The "${path}" query parameter value [${value}] can not be a date before March 21, 2006.`,
      );
    }

    return true;
  }),
];

/**
 * Check that the "startTime" field is before the "endTime" field.
 */
export const startTimeIsBeforeEndTime = [
  body('startTime').custom((startTime, { req }) => {
    const startTimeDate = new Date(startTime);
    const endTimeDate = new Date(req.body.endTime);

    if (startTimeDate > endTimeDate) {
      throw new Error(
        `"startTime" [${startTime}] must be before "endTime" [${req.body.endTime}].`,
      );
    }

    return true;
  }),
];

/**
 * Check that the "startTime" and "endTime" fields differ for at least 10 seconds.
 */
export const atLeastTenSeconds = [
  body('endTime').custom((endTime, { req }) => {
    const startTimeDate = new Date(req.body.startTime);
    const endTimeDate = new Date(endTime);

    if ((endTimeDate.getTime() - startTimeDate.getTime()) / 1000 <= 10) {
      throw new Error(
        `"endTime" [${endTime}] must be at least 10 seconds apart from "startTime" [${req.body.startTime}].`,
      );
    }

    return true;
  }),
];

export default [
  ...exists,
  ...ISO8601,
  ...beforeNow,
  ...after21March2006,
  ...startTimeIsBeforeEndTime,
  ...atLeastTenSeconds,
];
