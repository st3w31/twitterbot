import { mockValidationRequest } from '@mocks/utils.js';

import {
  ISO8601,
  after21March2006,
  atLeastTenSeconds,
  beforeNow,
  exists,
  startTimeIsBeforeEndTime,
} from './date-filter.validator.js';

describe('dateFilterValidator', () => {
  /** @type {Date} */
  let today;

  /** @type {Date} */
  let tomorrow;

  beforeAll(() => {
    jest.useFakeTimers('modern').setSystemTime(new Date('2022-01-01'));

    today = new Date().toISOString();
    tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000).toISOString();
  });

  afterAll(() => {
    jest.runOnlyPendingTimers();
    jest.useRealTimers();
  });

  describe('exists', () => {
    it('should fail when "startTime" and "endTime" do not exist.', async () => {
      const result = await mockValidationRequest({}, exists);

      expect(Object.keys(result)).toStrictEqual(['startTime', 'endTime']);
    });

    it('should fail when "startTime" does not exist.', async () => {
      const result = await mockValidationRequest(
        { endTime: 'not-falsy' },
        exists,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime']);
    });

    it('should fail when "endTime" does not exist.', async () => {
      const result = await mockValidationRequest(
        { startTime: 'not-falsy' },
        exists,
      );

      expect(Object.keys(result)).toStrictEqual(['endTime']);
    });

    it('should not fail when "startTime" and "endTime" does exist.', async () => {
      const result = await mockValidationRequest(
        { startTime: 'not-falsy', endTime: 'not-falsy' },
        exists,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('ISO8601', () => {
    /** @type {string[]} */
    let allowedFormats;

    beforeAll(() => {
      allowedFormats = [today, '2022-01-01T00:00:00.000+10:00'];
    });

    it('should fail when "startTime" and "endTime" are not valid ISO8601 date-time.', async () => {
      const result = await mockValidationRequest(
        { startTime: 'not-a-valid-format', endTime: 'not-a-valid-format' },
        ISO8601,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime', 'endTime']);
    });

    it('should fail when "startTime" is not a valid ISO8601 date-time.', async () => {
      const result = await mockValidationRequest(
        { startTime: 'not-a-valid-format', endTime: allowedFormats[0] },
        ISO8601,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime']);
    });

    it('should fail when "endTime" is not a valid ISO8601 date-time.', async () => {
      const result = await mockValidationRequest(
        { startTime: allowedFormats[1], endTime: 'not-a-valid-format' },
        ISO8601,
      );

      expect(Object.keys(result)).toStrictEqual(['endTime']);
    });

    it('should not fail when "startTime" and "endTime" are valid ISO8601 date-time.', async () => {
      const result = await mockValidationRequest(
        { startTime: allowedFormats[0], endTime: allowedFormats[1] },
        ISO8601,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('beforeNow', () => {
    it('should fail when "startTime" and "endTime" are future dates.', async () => {
      const result = await mockValidationRequest(
        { startTime: tomorrow, endTime: tomorrow },
        beforeNow,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime', 'endTime']);
    });

    it('should fail when "startTime" is a future date.', async () => {
      const result = await mockValidationRequest(
        { startTime: tomorrow, endTime: today },
        beforeNow,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime']);
    });

    it('should fail when "endTime" is a future date.', async () => {
      const result = await mockValidationRequest(
        { startTime: today, endTime: tomorrow },
        beforeNow,
      );

      expect(Object.keys(result)).toStrictEqual(['endTime']);
    });

    it('should not fail when "startTime" and "endTime" are not future dates.', async () => {
      const result = await mockValidationRequest(
        { startTime: today, endTime: today },
        beforeNow,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('after21March2006', () => {
    /** @type {Date} */
    let _20March2006;

    /** @type {Date} */
    let _21March2006;

    beforeAll(() => {
      _20March2006 = new Date('2006-03-20');
      _21March2006 = new Date('2006-03-21');
    });

    it('should fail when "startTime" and "endTime" are dates before March 21, 2006.', async () => {
      const result = await mockValidationRequest(
        { startTime: _20March2006, endTime: _20March2006 },
        after21March2006,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime', 'endTime']);
    });

    it('should fail when "startTime" is a date before March 21, 2006.', async () => {
      const result = await mockValidationRequest(
        { startTime: _20March2006, endTime: _21March2006 },
        after21March2006,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime']);
    });

    it('should fail when "endTime" is a date before March 21, 2006.', async () => {
      const result = await mockValidationRequest(
        { startTime: _21March2006, endTime: _20March2006 },
        after21March2006,
      );

      expect(Object.keys(result)).toStrictEqual(['endTime']);
    });

    it('should not fail when "startTime" and "endTime" are dates after March 21, 2006.', async () => {
      const result = await mockValidationRequest(
        { startTime: _21March2006, endTime: _21March2006 },
        after21March2006,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('startTimeIsBeforeEndTime', () => {
    it('should fail when "startTime" is after "endTime".', async () => {
      const result = await mockValidationRequest(
        { startTime: tomorrow, endTime: today },
        startTimeIsBeforeEndTime,
      );

      expect(Object.keys(result)).toStrictEqual(['startTime']);
    });

    it('should not fail when "startTime" is before "endTime".', async () => {
      const result = await mockValidationRequest(
        { startTime: today, endTime: tomorrow },
        startTimeIsBeforeEndTime,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('atLeastTenSeconds', () => {
    let todayPlusTenSecs;
    let todayPlusElevenSecs;

    beforeAll(() => {
      todayPlusTenSecs = new Date();
      todayPlusTenSecs.setSeconds(todayPlusTenSecs.getSeconds() + 10);

      todayPlusElevenSecs = new Date();
      todayPlusElevenSecs.setSeconds(todayPlusElevenSecs.getSeconds() + 11);
    });

    it('should fail when "endTime" is not 10 seconds apart from "startTime"', async () => {
      const result = await mockValidationRequest(
        { startTime: today, endTime: todayPlusTenSecs },
        atLeastTenSeconds,
      );

      expect(Object.keys(result)).toStrictEqual(['endTime']);
    });

    it('should not fail when "endTime" is 10 seconds apart from "startTime"', async () => {
      const result = await mockValidationRequest(
        { startTime: today, endTime: todayPlusElevenSecs },
        atLeastTenSeconds,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });
});
