import { body } from 'express-validator';
import squash from 'just-squash';
import { isString } from 'lodash';
import { isValidHashtag } from 'twitter-text';

/**
 * Check that the "keywords" field is an array.
 */
export const isArray = [
  body('keywords')
    .isArray({ min: 1, max: 10 })
    .withMessage(
      (_value, { path }) =>
        `The "${path}" query parameter length must be between 1 and 10.`,
    ),
];

/**
 * Sanitize all "keywords" field entries (remove whitespaces and duplicates #).
 */
export const sanitize = [
  body('keywords.*').customSanitizer((value) => {
    if (!isString(value)) {
      return null;
    }

    return '#' + squash(value.replaceAll('#', ''));
  }),
];

/**
 * Check that the "keywords" field contains only letters, numbers and underscores.
 */
export const regExp = [
  body('keywords.*').custom((value, { path }) => {
    if (!isValidHashtag(value)) {
      throw new Error(
        `The "${path}" query parameter can contain only letters, numbers and underscores.`,
      );
    }

    return true;
  }),
];

export default [...isArray, ...sanitize, ...regExp];
