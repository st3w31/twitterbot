import { isValidHashtag } from 'twitter-text';

import { mockSanitizerRequest, mockValidationRequest } from '@mocks/utils.js';

import { isArray, regExp, sanitize } from './hashtag.validator.js';

jest.mock('twitter-text', () => ({
  ...jest.requireActual('twitter-text'),
  isValidHashtag: jest.fn(),
}));

describe('hashtag', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  describe('isArray', () => {
    it('should fail when "hashtag" field array length is less than 1 or greater than 10.', async () => {
      const withZero = await mockValidationRequest({ keywords: [] }, isArray);

      const withEleven = await mockValidationRequest(
        { keywords: new Array(11).fill('test') },
        isArray,
      );

      expect(Object.keys(withZero)).toStrictEqual(['keywords']);
      expect(Object.keys(withEleven)).toStrictEqual(['keywords']);
    });

    it('should not fail when "keywords" field array length is between 1 and 10.', async () => {
      const withOne = await mockValidationRequest(
        { keywords: new Array(1).fill('test') },
        isArray,
      );

      const withTen = await mockValidationRequest(
        { keywords: new Array(10).fill('test') },
        isArray,
      );

      expect(Object.keys(withOne)).toStrictEqual([]);
      expect(Object.keys(withTen)).toStrictEqual([]);
    });
  });

  describe('sanitize', () => {
    it('should sanitize the values in the "keywords" field.', async () => {
      const result = await mockSanitizerRequest(
        { keywords: [' spaces spaces spaces ', '#multiple#hash#', 111] },
        sanitize,
      );

      expect(result.keywords[0]).toBe('#spacesspacesspaces');
      expect(result.keywords[1]).toBe('#multiplehash');
      expect(result.keywords[2]).toBe(null);
    });
  });

  describe('regExp', () => {
    it('should fail when "isValidHashtag" returns false.', async () => {
      isValidHashtag.mockReturnValue(false);
      const result = await mockValidationRequest(
        { keywords: ['hashtag'] },
        regExp,
      );

      expect(Object.keys(result)).toStrictEqual(['keywords[0]']);
    });

    it('should not fail when "isValidHashtag" returns true.', async () => {
      isValidHashtag.mockReturnValue(true);
      const result = await mockValidationRequest(
        { keywords: ['hashtag'] },
        regExp,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });
});
