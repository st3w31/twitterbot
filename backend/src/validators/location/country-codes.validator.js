import { body } from 'express-validator';

export const isArray = [
  body('countryCodes')
    .isArray({ min: 1, max: 10 })
    .withMessage(
      (_value, { path }) =>
        `The "${path}" query parameter array length must be between 1 and 10.`,
    ),
];

export const ISO31661Alpha2 = [
  body('countryCodes.*')
    .isISO31661Alpha2()
    .withMessage((value) => `"${value}" is not a valid ISO31661Alpha2 string.`),
];

export default [...isArray, ...ISO31661Alpha2];
