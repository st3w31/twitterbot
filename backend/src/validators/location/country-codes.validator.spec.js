import { mockValidationRequest } from '@mocks/utils.js';

import { ISO31661Alpha2, isArray } from './country-codes.validator.js';

describe('countryCodes', () => {
  describe('isArray', () => {
    it('should fail when "countryCodes" field array length is less than 1 or greater than 10.', async () => {
      const withZero = await mockValidationRequest(
        { countryCodes: [] },
        isArray,
      );

      const withEleven = await mockValidationRequest(
        { countryCodes: new Array(11).fill('h') },
        isArray,
      );

      expect(Object.keys(withZero)).toStrictEqual(['countryCodes']);
      expect(Object.keys(withEleven)).toStrictEqual(['countryCodes']);
    });

    it('should not fail when "countryCodes" field array length is between 1 and 10.', async () => {
      const withOne = await mockValidationRequest(
        { countryCodes: new Array(1).fill('h') },
        isArray,
      );

      const withTen = await mockValidationRequest(
        { countryCodes: new Array(10).fill('h') },
        isArray,
      );

      expect(Object.keys(withOne)).toStrictEqual([]);
      expect(Object.keys(withTen)).toStrictEqual([]);
    });
  });

  describe('ISO31661Alpha2', () => {
    it('should fail when "countryCodes" field contains invalid ISO31661Alpha2 string.', async () => {
      const result = await mockValidationRequest(
        { countryCodes: ['IT', 'not-valid', 'US', 'AL', 'not-valid'] },
        ISO31661Alpha2,
      );

      expect(Object.keys(result)).toStrictEqual([
        'countryCodes[1]',
        'countryCodes[4]',
      ]);
    });

    it('should not fail when "countryCodes" field contains valid ISO31661Alpha2 string.', async () => {
      const result = await mockValidationRequest(
        { countryCodes: ['IT', 'US', 'AL', 'PL'] },
        ISO31661Alpha2,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });
});
