import { mockValidationRequest } from '@mocks/utils.js';

import { isInt } from './max-results.validator.js';

describe('maxResults', () => {
  describe('isInt', () => {
    it('should fail when "maxResults" field is an integer smaller than 10 and higher than 500.', async () => {
      const with9 = await mockValidationRequest({ maxResults: 9 }, isInt);
      const with501 = await mockValidationRequest({ maxResults: 501 }, isInt);

      expect(Object.keys(with9)).toStrictEqual(['maxResults']);
      expect(Object.keys(with501)).toStrictEqual(['maxResults']);
    });

    it('should not fail when "maxResults" field is an integer between 10 and 500.', async () => {
      const with10 = await mockValidationRequest({ maxResults: 10 }, isInt);
      const with500 = await mockValidationRequest({ maxResults: 500 }, isInt);

      expect(Object.keys(with10)).toStrictEqual([]);
      expect(Object.keys(with500)).toStrictEqual([]);
    });
  });
});
