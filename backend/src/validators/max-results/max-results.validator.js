import { body } from 'express-validator';

export const isInt = [
  body(['maxResults'])
    .isInt({ min: 10, max: 500 })
    .withMessage(
      (value) => ` "${value}" must be an integer between 10 and 150.`,
    ),
];

export default [...isInt];
