import { query } from 'express-validator';

export const code = [
  query('code')
    .isString()
    .withMessage(
      (_value, { path }) =>
        `The "${path}" query parameter must be of type string.`,
    ),
];

export const state = [
  query('state')
    .isString()
    .withMessage(
      (_value, { path }) =>
        `The "${path}" query parameter must be of type string.`,
    ),
];

export default [...code, ...state];
