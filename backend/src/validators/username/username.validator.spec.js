import { mockValidationRequest } from '@mocks/utils.js';

import { exists, isLength, regExp } from './username.validator.js';

describe('username', () => {
  describe('exists', () => {
    it('should fail when the "username" field does not exist.', async () => {
      const result = await mockValidationRequest({}, exists);

      expect(Object.keys(result)).toStrictEqual(['username']);
    });

    it('should not fail when the "username" field does not exist.', async () => {
      const result = await mockValidationRequest(
        { username: 'username' },
        exists,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('regExp', () => {
    it('should fail when the "username" field contains characters that are not allowed.', async () => {
      const result = await mockValidationRequest(
        { username: 'this-username-contains-invalid-characters!"' },
        regExp,
      );

      expect(Object.keys(result)).toStrictEqual(['username']);
    });

    it('should not fail when the "username" field contains only letters, numbers and underscores.', async () => {
      const result = await mockValidationRequest(
        { username: 'this_is_a_valid_username0123456789' },
        regExp,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });

  describe('isLength', () => {
    it('should fail when the "username" field length is less than 4.', async () => {
      const result = await mockValidationRequest({ username: 'aaa' }, isLength);

      expect(Object.keys(result)).toStrictEqual(['username']);
    });

    it('should fail when the "username" field length is greater than 15.', async () => {
      const result = await mockValidationRequest(
        { username: 'username-too-long' },
        isLength,
      );

      expect(Object.keys(result)).toStrictEqual(['username']);
    });

    it('should not fail when the "username" field length is between 4 and 15.', async () => {
      const result = await mockValidationRequest(
        { username: 'valid-username' },
        isLength,
      );

      expect(Object.keys(result)).toStrictEqual([]);
    });
  });
});
