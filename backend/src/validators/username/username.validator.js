import { body } from 'express-validator';

/**
 * Check that the "username" field exists.
 */
export const exists = [
  body('username')
    .exists({ checkFalsy: true })
    .withMessage(
      (_value, { path }) => `The "${path}" query parameter can not be empty.`,
    ),
];

/**
 * Check that the "username" field contains only letters, numbers and underscores.
 */
export const regExp = [
  body('username')
    .matches('^[a-zA-Z0-9_]*$')
    .withMessage(
      (_value, { path }) =>
        `The "${path}" query parameter can contain only letters, numbers and underscores.`,
    ),
];

/**
 * Check that the "username" field length is between 4 and 15.
 */
export const isLength = [
  body('username')
    .isLength({ min: 4, max: 15 })
    .withMessage(
      (_value, { path }) =>
        `The "${path}" query parameter length must be between 4 and 15.`,
    ),
];

export default [...exists, ...regExp, ...isLength];
