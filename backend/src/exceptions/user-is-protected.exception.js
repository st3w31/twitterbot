import { BadRequest } from 'http-errors';

export class UserIsProtectedException extends BadRequest {
  constructor(username) {
    super(`@${username}'s tweets are protected.`);
  }
}
