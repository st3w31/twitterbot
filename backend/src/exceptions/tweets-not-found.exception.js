import { BadRequest } from 'http-errors';

export class TweetsNotFoundException extends BadRequest {
  constructor() {
    super('No tweets match this search criteria.');
  }
}
