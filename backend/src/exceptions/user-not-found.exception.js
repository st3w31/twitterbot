import { BadRequest } from 'http-errors';

export class UserNotFoundException extends BadRequest {
  constructor(username) {
    super(`@${username} does not exists.`);
  }
}
