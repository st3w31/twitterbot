import createHttpError from 'http-errors';

import mockClient from '@mocks/config/bearer-client.js';
import { mockUser } from '@mocks/v2/user.js';

import getUserByUsername from './get-user-by-username.js';

jest.mock('@/config/bearer-client.js');

describe('getUserByUsername', () => {
  it('should return the user.data', async () => {
    const user = mockUser();
    mockClient.users.findUserByUsername.mockResolvedValue({ data: user });
    await expect(getUserByUsername('username')).resolves.toEqual(user);
  });

  it('should throw an error when the user was not found.', async () => {
    mockClient.users.findUserByUsername.mockResolvedValue({});
    await expect(getUserByUsername('username')).rejects.toThrowError(
      createHttpError.BadRequest,
    );
  });
});
