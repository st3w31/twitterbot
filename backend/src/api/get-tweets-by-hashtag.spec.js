import createError from 'http-errors';

import mockClient from '@mocks/config/bearer-client.js';

import getTweetsByHashtag from './get-tweets-by-hashtag.js';

describe('getTweetsByHashtag', () => {
  let keywords;
  let dateFilter;

  beforeAll(() => {
    keywords = ['test'];
    dateFilter = {
      startTime: '2021-10-27T00:00:00.000Z',
      endTime: '2021-10-28T00:00:00.000Z',
    };
  });

  it('should throw an error when result_count is 0', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      meta: { result_count: 0 },
    });

    await expect(
      getTweetsByHashtag(keywords, dateFilter),
    ).rejects.toBeInstanceOf(createError.BadRequest);
  });

  it('should return an object that contains an array of tweets', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      data: [
        {
          id: 'id',
          text: 'text',
          edit_history_tweet_ids: 'edit_history_tweet_ids',
          created_at: 'created_at',
          public_metrics: 'public_metrics',
        },
      ],
      meta: { result_count: 1 },
    });

    await expect(getTweetsByHashtag(keywords, dateFilter)).resolves.toEqual({
      tweets: [
        {
          id: 'id',
          text: 'text',
          edit_history_tweet_ids: 'edit_history_tweet_ids',
          created_at: 'created_at',
          public_metrics: 'public_metrics',
        },
      ],
    });
  });
});
