import client from '@/config/bearer-client.js';
import { TweetsNotFoundException } from '@/exceptions/tweets-not-found.exception.js';
import { UserIsProtectedException } from '@/exceptions/user-is-protected.exception.js';
import {
  mediaFields,
  placeFields,
  tweetFields,
  userFields,
} from '@/utils/fields.js';
import { f } from '@/utils/utils.js';

import getUserByUsername from './get-user-by-username.js';

/**
 * Get user tweets.
 *
 * @param {string} username User username.
 * @param {DateFilter} dateFilter Date filter.
 * @param {Number} maxResults Max results.
 * @returns {Object} Tweets.
 */
export default async function getUserTweets(username, dateFilter, maxResults) {
  const user = await getUserByUsername(username); //NOSONAR

  if (user?.protected) {
    throw new UserIsProtectedException(username);
  }

  const tweets = await client.tweets.tweetsFullarchiveSearch({
    query: `from:${user.username} -is:retweet -is:reply`,
    start_time: dateFilter.startTime,
    end_time: dateFilter.endTime,
    max_results: maxResults,
    'tweet.fields': tweetFields,
    'place.fields': placeFields,
    'user.fields': userFields,
    'media.fields': mediaFields,
    expansions: ['author_id', 'geo.place_id', 'attachments.media_keys'],
  });

  if (tweets?.meta?.result_count === 0) {
    throw new TweetsNotFoundException();
  }

  return f(tweets);
}
