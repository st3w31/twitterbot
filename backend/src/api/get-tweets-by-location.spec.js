import createError from 'http-errors';
import { cloneDeep, omit } from 'lodash';

import mockClient from '@mocks/config/bearer-client.js';
import { mockPlace } from '@mocks/v2/place.js';
import { mockTweet } from '@mocks/v2/tweet.js';
import { mockUser } from '@mocks/v2/user.js';

import getTweetsByLocation from './get-tweets-by-location';

describe('getTweetsByLocation', () => {
  let tweet;
  let user;
  let place;

  beforeAll(() => {
    tweet = mockTweet();
    user = mockUser();
    place = mockPlace();
  });

  it('should throw a bad request error when no tweets were found.', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      meta: { result_count: 0 },
    });

    await expect(
      getTweetsByLocation(
        { countryCodes: ['IT'] },
        { startTime: '', endTime: '' },
      ),
    ).rejects.toBeInstanceOf(createError.BadRequest);
  });

  it('should return the formatted response.', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      data: [tweet],
      includes: { users: [user], places: [place] },
      meta: { result_count: 1 },
    });

    const response = { tweets: [tweet], users: {}, places: {} };
    response.users[user.id] = cloneDeep(omit(user, 'id'));
    response.places[place.id] = cloneDeep(omit(place, 'id'));

    await expect(
      getTweetsByLocation(
        { countryCodes: ['IT'] },
        { startTime: '', endTime: '' },
      ),
    ).resolves.toEqual(response);
  });
});
