import client from '@/config/bearer-client.js';
import { TweetsNotFoundException } from '@/exceptions/tweets-not-found.exception.js';
import {
  mediaFields,
  placeFields,
  tweetFields,
  userFields,
} from '@/utils/fields.js';
import { buildQuery, f } from '@/utils/utils.js';

/**
 * Get tweets by location.
 *
 * @param {Location} location Location.
 * @param {DateFilter} dateFilter Date filter.
 * @param {Number} maxResults Max results.
 */
export default async function getTweetsByLocation(
  location,
  dateFilter,
  maxResults,
) {
  const { countryCodes } = location;
  const { startTime, endTime } = dateFilter;

  const tweets = await client.tweets.tweetsFullarchiveSearch({
    // See: https://developer.twitter.com/en/docs/tutorials/filtering-tweets-by-location
    // "Retweets can not have a Place attached to them, so if you use an operator
    // such as has:geo, you will not match any Retweets"
    query: `has:geo -is:reply (${buildQuery('place_country', countryCodes)})`,
    start_time: startTime,
    end_time: endTime,
    max_results: maxResults,
    'tweet.fields': tweetFields,
    'place.fields': placeFields,
    'user.fields': userFields,
    'media.fields': mediaFields,
    expansions: ['author_id', 'geo.place_id', 'attachments.media_keys'],
  });

  if (tweets?.meta?.result_count === 0) {
    throw new TweetsNotFoundException();
  }
  return f(tweets);
}
