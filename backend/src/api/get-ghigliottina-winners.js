import createHttpError from 'http-errors';

import client from '@/config/bearer-client.js';

export default async function getGhigliottinaWinners(dateFilter) {
  const tagSentence = 'campioni più veloci della #ghigliottina sono: ';
  const tweets = await client.tweets.tweetsFullarchiveSearch({
    query: `from:quizzettone "${tagSentence}"`,
    start_time: dateFilter.startTime,
    end_time: dateFilter.endTime,
  });

  if (tweets?.meta?.result_count === 0) {
    throw new createHttpError(400, 'No winners on this date.');
  }
  const winners = tweets.data[0].text.match(
    /@[a-zA-Z0-9]+ - \d{2}:\d{2}:\d{2}/g,
  );
  if (winners == null) {
    throw new createHttpError(400, 'No winners on this date.');
  }
  
  const data = winners.map((curr) => {
    const splt = curr.split('-');
    return { user: splt[0].trim(), time: splt[1].trim() };
  });
  return { data };
}
