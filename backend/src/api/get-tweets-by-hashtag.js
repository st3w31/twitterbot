import client from '@/config/bearer-client.js';
import { TweetsNotFoundException } from '@/exceptions/tweets-not-found.exception.js';
import {
  mediaFields,
  placeFields,
  tweetFields,
  userFields,
} from '@/utils/fields.js';
import { f } from '@/utils/utils.js';

/**
 * Get tweets by hashtag.
 *
 * @param {Hashtag[]} keywords Hashtag array.
 * @param {DateFilter} dateFilter Date filter.
 * @param {Number} maxResults Max results.
 * @returns {Object} response data object.
 */
export default async function getTweetsByHashtag(
  keywords,
  dateFilter,
  maxResults,
) {
  const tweets = await client.tweets.tweetsFullarchiveSearch({
    query: `(${keywords.join(' OR ')}) -is:retweet -is:reply`,
    start_time: dateFilter.startTime,
    end_time: dateFilter.endTime,
    max_results: maxResults,
    'tweet.fields': tweetFields,
    'user.fields': userFields,
    'place.fields': placeFields,
    'media.fields': mediaFields,
    expansions: ['author_id', 'geo.place_id', 'attachments.media_keys'],
  });

  if (tweets?.meta?.result_count === 0) {
    throw new TweetsNotFoundException();
  }
  return f(tweets);
}
