import createError from 'http-errors';

import mockClient from '@mocks/config/bearer-client.js';

import getGhigliottinaWinners from './get-ghigliottina-winners.js';

describe('getShigliottinaWinners', () => {
  let dateFilter;
  let textTweet;
  let result;

  beforeAll(() => {
    textTweet =
      'Per #leredità su Twitter, i campioni più veloci della #ghigliottina sono:🥇 @carlina2465 - 19:18:37🥈 @climottantasei - 19:18:46🥉 @CatMoscow57 - 19:18:51';

    result = {
      data: [
        { user: '@carlina2465', time: '19:18:37' },
        { user: '@climottantasei', time: '19:18:46' },
        { user: '@CatMoscow57', time: '19:18:51' },
      ],
    };
    dateFilter = {
      startTime: '2022-12-08T23:00:00.000Z',
      endTime: '2022-12-09T22:59:59.999Z',
    };
  });

  beforeEach(() => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReset();
  });

  it('should throw an error when result_count is 0', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      meta: { result_count: 0 },
    });

    await expect(getGhigliottinaWinners(dateFilter)).rejects.toBeInstanceOf(
      createError.BadRequest,
    );
  });

  it('should throw an error when text doesnt match regex 1', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      data: [{ text: 'no match' }],
      meta: { result_count: 1 },
    });

    await expect(getGhigliottinaWinners(dateFilter)).rejects.toBeInstanceOf(
      createError.BadRequest,
    );
  });
  it('should return an array of objects', async () => {
    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      data: [
        {
          id: 'id',
          text: textTweet,
          edit_history_tweet_ids: 'edit_history_tweet_ids',
          created_at: 'created_at',
          public_metrics: 'public_metrics',
        },
      ],
      meta: { result_count: 1 },
    });

    await expect(getGhigliottinaWinners(dateFilter)).resolves.toEqual(result);
  });
});
