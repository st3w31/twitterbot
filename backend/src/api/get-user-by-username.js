import client from '@/config/bearer-client.js';
import { UserNotFoundException } from '@/exceptions/user-not-found.exception.js';
import { userFields } from '@/utils/fields.js';

/**
 * Get user by his username.
 *
 * @param {string} username User username.
 * @returns {Object} User object.
 */
export default async function getUserByUsername(username) {
  const { data } = await client.users.findUserByUsername(username, {
    'user.fields': userFields,
  });

  if (!data) {
    throw new UserNotFoundException(username);
  }

  return data;
}
