import createError from 'http-errors';
import { cloneDeep, omit } from 'lodash';

import mockClient from '@mocks/config/bearer-client.js';
import { mockPlace } from '@mocks/v2/place.js';
import { mockTweet } from '@mocks/v2/tweet.js';
import { mockUser } from '@mocks/v2/user.js';

import getUserByUsername from './get-user-by-username.js';
import getUserTweets from './get-user-tweets.js';

jest.mock('./get-user-by-username.js');

describe('getUserTweets', () => {
  let user;
  let place;
  let tweet;
  let dateFilter;

  beforeAll(() => {
    user = mockUser({ _protected: false });
    place = mockPlace();
    tweet = mockTweet({ authorId: user.id });
    dateFilter = { startTime: '', endTime: '' };
  });

  it('should throw a bad request error when the user is protected.', async () => {
    getUserByUsername.mockReturnValue(mockUser({ _protected: true }));

    await expect(
      getUserTweets(user.username, dateFilter),
    ).rejects.toBeInstanceOf(createError.BadRequest);
  });

  it('should throw a bad request error when no tweets were found.', async () => {
    getUserByUsername.mockReturnValue(user);

    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      meta: { result_count: 0 },
    });

    await expect(
      getUserTweets(user.username, dateFilter),
    ).rejects.toBeInstanceOf(createError.BadRequest);
  });

  it('should return the formatted response.', async () => {
    getUserByUsername.mockReturnValue(user);

    mockClient.tweets.tweetsFullarchiveSearch.mockReturnValue({
      data: [tweet],
      includes: { users: [user], places: [place] },
      meta: { result_count: 1 },
    });

    const response = { tweets: [tweet], users: {}, places: {} };
    response.users[user.id] = cloneDeep(omit(user, 'id'));
    response.places[place.id] = cloneDeep(omit(place, 'id'));

    await expect(getUserTweets(user.username, dateFilter)).resolves.toEqual(
      response,
    );
  });
});
