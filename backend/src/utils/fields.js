export const userFields = [
  'id',
  'name',
  'username',
  'profile_image_url',
  'protected',
  'verified',
].toString();

export const tweetFields = [
  'id',
  'text',
  'edit_history_tweet_ids',
  'author_id',
  'created_at',
  'public_metrics',
].toString();

export const placeFields = [
  'id',
  'full_name',
  'country',
  'country_code',
  'place_type',
  'geo',
].toString();

export const mediaFields = [
  'url',
  'alt_text',
  'type',
  'width',
  'height',
].toString();
