import { placeFields, tweetFields, userFields } from './fields.js';

describe('fields', () => {
  it('should match userFields array.', () => {
    expect(userFields).toEqual(
      'id,name,username,profile_image_url,protected,verified',
    );
  });

  it('should match tweetFields array.', () => {
    expect(tweetFields).toEqual(
      'id,text,edit_history_tweet_ids,author_id,created_at,public_metrics',
    );
  });

  it('should match placeFields array.', () => {
    expect(placeFields).toEqual(
      'id,full_name,country,country_code,place_type,geo',
    );
  });
});
