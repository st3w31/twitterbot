import assert from 'node:assert/strict';

/**
 * Build query.
 *
 * @param {string} operator Operator.
 * @param {string[]} values Values.
 * @return {string} Query.
 */
export function buildQuery(operator, values) {
  assert(typeof operator === 'string');
  assert(Array.isArray(values));
  assert(values.every((value) => typeof value === 'string'));

  let query = '';

  values.forEach((value, index) => {
    query += operator + ':' + value;

    if (index !== values.length - 1) {
      query += ' OR ';
    }
  });

  return query;
}

/**
 * Format twitter response.
 *
 * @param {Object} raw Raw response.
 * @returns Formatted response.
 */
export function f(raw) {
  assert(typeof raw === 'object');
  assert(Array.isArray(raw.data));

  const pretty = {};

  const addField = (name, array) => {
    assert(Array.isArray(array));
    assert(
      array.every(
        (value) =>
          (value.id && typeof value.id === 'string') ||
          (value.media_key && typeof value.media_key === 'string'),
      ),
    );

    pretty[name] = {};
    array.forEach((value) => {
      if (value.id) {
        const { id, ...properties } = value;
        pretty[name][id] = properties;
      } else if (value.media_key) {
        const { media_key, ...properties } = value;
        pretty[name][media_key] = properties;
      }
    });
  };

  if (raw?.includes?.users) {
    addField('users', raw.includes.users);
  }

  if (raw?.includes?.places) {
    addField('places', raw.includes.places);
  }

  if (raw?.includes?.media) {
    addField('media', raw.includes.media);
  }

  pretty['tweets'] = raw.data;

  return pretty;
}
