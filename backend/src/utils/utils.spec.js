import { cloneDeep, omit } from 'lodash';
import { AssertionError } from 'node:assert/strict';

import { mockPlace } from '@mocks/v2/place.js';
import { mockTweet } from '@mocks/v2/tweet.js';
import { mockUser } from '@mocks/v2/user.js';

import { buildQuery, f } from './utils.js';

describe('utils', () => {
  describe('buildQuery', () => {
    it('should not build a query when the "operator" parameter is not a string.', () => {
      expect(() => buildQuery()).toThrow(AssertionError);
      expect(() => buildQuery(['not-a-string'])).toThrow(AssertionError);
    });

    it('should not build a query when the "values" parameter is not an array.', () => {
      expect(() => buildQuery('valid', 'not-an-array')).toThrow(AssertionError);
    });

    it('should not build a query when the "values" parameter is not an array of string.', () => {
      expect(() => buildQuery('valid', ['hello', 12])).toThrow(AssertionError);
    });

    it('should build a query when all parameters are valid.', () => {
      expect(buildQuery('place_country', ['IT', 'US', 'AL'])).toBe(
        'place_country:IT OR place_country:US OR place_country:AL',
      );
    });
  });

  describe('f', () => {
    let user;
    let tweet;
    let place;

    beforeAll(() => {
      user = mockUser();
      tweet = mockTweet();
      place = mockPlace();
    });

    it('should fail when "raw" parameter is not an object.', () => {
      expect(() => f('not-an-object')).toThrow(AssertionError);
    });

    it('should fail when "raw.data" field is not an array.', () => {
      expect(() => f({})).toThrow(AssertionError);
      expect(() => f({ data: 'not-an-array' })).toThrow(AssertionError);
    });

    it('should fail when "raw.includes.users" is not an array.', () => {
      expect(() =>
        f({
          data: [],
          includes: { users: 'not-an-array' },
        }),
      ).toThrow(AssertionError);
    });

    it('should fail when "raw.includes.places" is not an array.', () => {
      expect(() => {
        f({
          data: [],
          includes: { places: 'not-an-array' },
        });
      }).toThrow(AssertionError);
    });

    it('should fail when an object of the "raw.includes.users" array does not contain an "id" field.', () => {
      const userWithoutId = cloneDeep(omit(user, 'id'));

      expect(() =>
        f({
          data: [],
          includes: { users: [userWithoutId] },
        }),
      ).toThrow(AssertionError);
    });

    it('should fail when an object of the "raw.includes.places" array does not contain an "id" field.', () => {
      const placeWithoutId = cloneDeep(omit(place, 'id'));

      expect(() =>
        f({
          data: [],
          includes: { places: [placeWithoutId] },
        }),
      ).toThrow(AssertionError);
    });

    it('should return the formatted response (without users and places).', () => {
      const raw = { data: [tweet] };
      const response = { tweets: raw.data };

      expect(f(raw)).toStrictEqual(response);
    });

    it('should return the formatted response (with users and places).', () => {
      const raw = {
        data: [tweet],
        includes: { users: [user], places: [place] },
      };

      const { id: userId, ...userProperties } = user;
      const { id: placeId, ...placeProperties } = place;

      const response = { tweets: raw.data, users: {}, places: {} };
      response.users[userId] = userProperties;
      response.places[placeId] = placeProperties;

      expect(f(raw)).toStrictEqual(response);
    });
  });
});
