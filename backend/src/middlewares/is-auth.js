import { Unauthorized } from 'http-errors';

import { authClientFromToken } from '@/config/auth-client.js';

/**
 * Authentication middleware.
 *
 * @param {import('express').Request} _req Request.
 * @param {import('express').Response} res Response.
 * @param {import('express').NextFunction} next Next.
 */
export default function isAuth(req, _res, next) {
  if (
    !req.session?.token ||
    authClientFromToken(req.session.token).isAccessTokenExpired()
  ) {
    return next(
      Unauthorized('Only authenticated users can access this route.'),
    );
  }

  next();
}
