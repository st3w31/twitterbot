import createError from 'http-errors';

/**
 * Not found error handler.
 *
 * @param {import('express').Request} _req Request.
 * @param {import('express').Response} _res Response.
 * @param {import('express').NextFunction} next Next.
 */
export default function notFoundErrorHandler(_req, _res, next) {
  next(createError(404, 'The route does not exist.'));
}
