import createHttpError from 'http-errors';
import { createRequest, createResponse } from 'node-mocks-http';

import errorHandler from './error-handler.js';

describe('errorHandler', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    req = createRequest();
    res = createResponse();
    next = jest.fn();
  });

  it('should handle "http-errors" which do not contain an "errors" field.', () => {
    errorHandler(new createHttpError(400, 'bad request'), req, res, next);

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBe(true);

    const data = res._getJSONData();
    expect(data.message).toBe('bad request');
    expect(data.errors).toBeFalsy();
  });

  it('should handle "http-errors" which contains an "errors" field.', () => {
    errorHandler(
      new createHttpError(400, { message: 'bad request', errors: {} }),
      req,
      res,
      next,
    );

    expect(res.statusCode).toBe(400);
    expect(res._isJSON()).toBe(true);

    const data = res._getJSONData();
    expect(data.message).toBe('bad request');
    expect(data.errors).toStrictEqual({});
  });

  it('should throw an "Internal Server Error" when the "error" parameter has an unknown type.', () => {
    errorHandler(new Error(), req, res, next);

    expect(res.statusCode).toBe(500);
    expect(res._isJSON()).toBe(true);

    const data = res._getJSONData();
    expect(data.message).toBe('Internal Server Error');
  });
});
