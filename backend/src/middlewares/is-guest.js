import { Unauthorized } from 'http-errors';

import { authClientFromToken } from '@/config/auth-client.js';

/**
 * Guest middleware.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} res Response.
 * @param {import('express').NextFunction} next Next.
 */
export default function isGuest(req, _res, next) {
  if (
    req.session?.token &&
    !authClientFromToken(req.session.token).isAccessTokenExpired()
  ) {
    return next(Unauthorized('Only guest users can access this route.'));
  }

  return next();
}
