import { isHttpError } from 'http-errors';

/**
 * Error handler.
 *
 * @param {any} error Error.
 * @param {import('express').Request} _req Request.
 * @param {import('express').Response} res Response.
 * @param {import('express').NextFunction} _next Next.
 * @returns Response.
 */
// eslint-disable-next-line no-unused-vars
export default function errorHandler(error, _req, res, _next) {
  if (isHttpError(error)) {
    return res.status(error.status).json({
      message: error.message,
      ...('errors' in error ? { errors: error.errors } : {}),
    });
  }

  res.status(500).json({ message: 'Internal Server Error' });
}
