import { createRequest, createResponse } from 'node-mocks-http';

import { validationResult } from '@mocks/vendor/express-validator.js';

import validationErrorHandler from './validation-error-handler.js';

describe('validationErrorHandler', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    req = createRequest();
    res = createResponse();
    next = jest.fn();
  });

  it('should call "next" with no arguments when there is no validation error.', () => {
    validationResult.mockImplementation(() => ({
      isEmpty: jest.fn().mockReturnValue(true),
    }));

    validationErrorHandler(req, res, next);
    expect(next).toHaveBeenCalled();
  });

  it('should call "next" with a 400 error which includes "errors".', () => {
    validationResult.mockImplementation(() => ({
      isEmpty: jest.fn().mockReturnValue(false),
      mapped: jest.fn().mockReturnValue([]),
    }));

    validationErrorHandler(req, res, next);
    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({
        status: 400,
        errors: [],
      }),
    );
  });
});
