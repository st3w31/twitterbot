import { validationResult } from 'express-validator';
import createError from 'http-errors';

/**
 * Validation error handler.
 *
 * @param {import('express').Request} req Request.
 * @param {import('express').Response} _res Response.
 * @param {import('express').NextFunction} next Next.
 */
export default function validationErrorHandler(req, _res, next) {
  const result = validationResult(req);

  if (!result.isEmpty()) {
    return next(createError(400, { errors: result.mapped() }));
  }

  next();
}
