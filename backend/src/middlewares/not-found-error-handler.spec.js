import { createRequest, createResponse } from 'node-mocks-http';

import notFoundErrorHandler from './not-found-error-handler.js';

describe('notFoundErrorHandler', () => {
  let req;
  let res;
  let next;

  beforeEach(() => {
    req = createRequest();
    res = createResponse();
    next = jest.fn();
  });

  it('should call "next" with a 404 error.', () => {
    notFoundErrorHandler(req, res, next);

    expect(next).toHaveBeenCalledWith(
      expect.objectContaining({
        status: 404,
        message: 'The route does not exist.',
      }),
    );
  });
});
