import { Router } from '@root/async-router';
import connectRedis from 'connect-redis';
import cors from 'cors';
import express from 'express';
import session from 'express-session';
import helmet from 'helmet';

import redisClient from './config/redis.js';
import errorHandler from './middlewares/error-handler.js';
import notFoundErrorHandler from './middlewares/not-found-error-handler.js';
import apiRoutes from './routes/api.js';
import authRoutes from './routes/auth.js';
import chessRoutes from './routes/chess.js';
import { isProd } from './utils/env.js';

export default function App() {
  const app = express();
  const router = Router();
  const RedisStore = connectRedis(session);

  app.use(helmet());
  app.use(
    cors({
      origin: process.env.CLIENT_BASE_URL,
      credentials: true,
    }),
  );
  app.use(express.json());
  app.use(
    session({
      name: 'sid',
      store: new RedisStore({ client: redisClient }),
      secret: process.env.SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: isProd() ? true : 'auto',
        maxAge: 7 * 24 * 60 * 60 * 1000,
      },
    }),
  );
  app.use('/api/v1', apiRoutes(router));
  app.use('/api/v1', authRoutes(router));
  app.use('/api/v1', chessRoutes(router));
  app.use(notFoundErrorHandler);
  app.use(errorHandler);

  return app;
}
