jest.mock('express-validator', () => {
  return {
    ...jest.requireActual('express-validator'),
    matchedData: jest.fn(),
    validationResult: jest.fn(),
  };
});

export * from 'express-validator';
