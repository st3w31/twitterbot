import authClient from '@/config/auth-client.js';

jest.mock('@/config/auth-client.js', () => {
  return {
    isAccessTokenExpired: jest.fn(),
    generateAuthURL: jest.fn(),
    requestAccessToken: jest.fn(),
    revokeAccessToken: jest.fn(),
  };
});

export default authClient;
