import Client from '@/config/bearer-client.js';

jest.mock('@/config/bearer-client.js', () => {
  return {
    users: { findUserByUsername: jest.fn() },
    tweets: { tweetsFullarchiveSearch: jest.fn() },
  };
});

export default Client;
