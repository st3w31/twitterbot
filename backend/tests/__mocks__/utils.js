import { validationResult } from 'express-validator';
import { createMocks } from 'node-mocks-http';

/**
 * Allows to test express validator middlewares.
 *
 * @param {Object} body Request body.
 * @param {Array} middlewares Array of validation middlewares.
 * @returns {Promise<Record<string, any>>} Errors.
 */
export async function mockValidationRequest(body, middlewares) {
  const { req, res } = createMocks({ body });
  await runMiddlewares(req, res, middlewares);
  return validationResult(req).mapped();
}

/**
 * Allows to test express validator sanitizers.
 *
 * @param {Object} body Request body.
 * @param {Array} middlewares Array of sanitizers middlewares.
 * @returns {Promise<Record<string, any>>} Errors.
 */
export async function mockSanitizerRequest(body, middlewares) {
  const { req, res } = createMocks({ body });
  await runMiddlewares(req, res, middlewares);
  return req.body;
}

async function runMiddlewares(req, res, middlewares) {
  await Promise.all(
    middlewares.map(async (middleware) => {
      await middleware(req, res, () => undefined);
    }),
  );
}
