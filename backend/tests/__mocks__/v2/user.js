import { faker } from '@faker-js/faker';

export function mockUser({
  id,
  name,
  username,
  profileImageUrl,
  _protected,
  verified,
} = {}) {
  faker.seed(0);

  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();

  return {
    id: id ?? faker.random.numeric(10),
    name: name ?? faker.name.fullName({ firstName, lastName }),
    username: username ?? faker.internet.userName(firstName, lastName),
    profile_image_url: profileImageUrl ?? faker.image.imageUrl(),
    protected: _protected ?? faker.datatype.boolean(),
    verified: verified ?? faker.datatype.boolean(),
  };
}
