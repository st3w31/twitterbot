import { faker } from '@faker-js/faker';

export function mockPlace({
  id,
  fullName,
  country,
  countryCode,
  placeType,
  geo,
} = {}) {
  faker.seed(0);

  return {
    id: id ?? faker.random.numeric(19),
    full_name: fullName ?? faker.address.cityName(),
    country: country ?? faker.address.country(),
    country_code: countryCode ?? faker.address.countryCode('alpha-2'),
    place_type: placeType ?? 'city',
    geo: geo ?? {
      type: 'Feature',
      bbox: [
        /* eslint-disable */
        faker.address.longitude(), // west_long
        faker.address.latitude(), // south_lat
        faker.address.longitude(), // east_long
        faker.address.latitude(), // north_lat
        /* eslint-enable */
      ],
      properties: {},
    },
  };
}
