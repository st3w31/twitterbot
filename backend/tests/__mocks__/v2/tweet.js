import { faker } from '@faker-js/faker';

export function mockTweet({
  id,
  text,
  editHistoryTweetIds,
  authorId,
  createdAt,
  publicMetrics,
} = {}) {
  faker.seed(0);

  return {
    id: id ?? faker.random.numeric(19),
    text: text ?? faker.lorem.words(10),
    edit_history_tweet_ids:
      editHistoryTweetIds ??
      faker.helpers.uniqueArray(faker.random.numeric(19), 2),
    author_id: authorId ?? faker.random.numeric(10),
    created_at: createdAt ?? faker.date.between(),
    public_metrics: publicMetrics ?? {
      retweet_count: faker.datatype.number(100),
      reply_count: faker.datatype.number(100),
      like_count: faker.datatype.number(100),
      quote_count: faker.datatype.number(100),
    },
  };
}
