type PlaceCountry = string;

interface Location {
  countryCodes?: PlaceCountry[];
}
