declare global {
  namespace NodeJS {
    interface ProcessEnv {
      PORT: string;
      JWT_SECRET: string;
      COOKIE_SECRET: string;
      TWITTER_CLIENT_ID: string;
      TWITTER_CLIENT_SECRET: string;
      TWITTER_CALLBACK_URL: string;
      TWITTER_BEARER_TOKEN: string;
      CLIENT_BASE_URL: string;
    }
  }
}

export {};
