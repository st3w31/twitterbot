const config = {
  presets: [
    [
      '@babel/env',
      {
        targets: {
          node: '18',
        },
      },
    ],
  ],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          '@': './src',
          '@mocks': './tests/__mocks__',
        },
      },
    ],
  ],
};

if (process.env.NODE_ENV === 'production') {
  config.plugins.push('babel-plugin-unassert');
}

module.exports = config;
