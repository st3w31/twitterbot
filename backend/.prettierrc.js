/** @type {import('prettier').Config} */
module.exports = {
  // prettier
  singleQuote: true,
  trailingComma: 'all',

  // @trivago/prettier-plugin-sort-imports
  importOrder: ['^@/(.*)$', '^@mocks/(.*)$', '^[./]'],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
  importOrderGroupNamespaceSpecifiers: true,
};
