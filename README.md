# TwitterBot

## Description
SWE Project @Unibo

## Environment Variables

To run this project, you will need to add environment variables to your .env file and following .env.example file in `backend` and `frontend`.

## With docker

### Building 
 * Run	`./build.sh`

### Start
 * Run	`docker compose up -d`

### Stop
 * Run `docker compose down`

## Without docker

### Install dependencies
Assuming node version: ^18.0

In the root directory
 * Run `yarn`

Move in backend directory
 * Run `yarn`

Move in frontend directory
 * Run `yarn` 

### Start

In backend directory
* Run `yarn run start`

In frontend directory
* Run `yarn run dev`

The application will start on `localhost:5173`, open it in the browser

## Authors
 * Enrico Emiro
 * Federico Gherardi
 * Filippo Speziali
 * Giulio Billi
 * Stefano Staffolani

