#!/bin/bash

set -e

docker build ./backend -t team1/node-backend
docker build ./frontend -t team1/node-frontend

