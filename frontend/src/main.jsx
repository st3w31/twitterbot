import 'flowbite';
import 'linkify-plugin-hashtag';
import 'linkify-plugin-mention';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';

import { AuthProvider } from './hooks/AuthProvider';
import './index.css';
import router from './router';

ReactDOM.createRoot(document.getElementById('root')).render(
  <AuthProvider>
    <RouterProvider router={router} />
  </AuthProvider>,
);
