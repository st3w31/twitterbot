import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';

import TwitterLogo from './TwitterLogo';

describe('TwitterLogo', () => {
  const imageUrl = '/images/twitter.png';
  test('src contains right path', () => {
    render(<TwitterLogo />);
    const testImage = screen.getByRole('img');
    expect(testImage).toHaveAttribute('src', imageUrl);
  });

  test('show image when it is loaded', () => {
    render(<TwitterLogo />);
    const testImage = screen.getByRole('img');
    expect(testImage).toBeInTheDocument();
  });
});
