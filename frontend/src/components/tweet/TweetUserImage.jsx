import PropTypes from 'prop-types';

function TweetUserImage({ profile_image_url }) {
  return (
    <img
      className="h-10 w-10 rounded-full"
      src={profile_image_url.replace('normal', '400x400')}
      alt=""
    />
  );
}

TweetUserImage.propTypes = {
  profile_image_url: PropTypes.string.isRequired,
};

export default TweetUserImage;
