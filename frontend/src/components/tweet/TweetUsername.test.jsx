import { render, screen } from '@testing-library/react';
import { describe, expect, it } from 'vitest';

import TweetUsername from './TweetUsername';

describe('TweetUsername', () => {
  it('should show username ', () => {
    const username = 'elonmusk';
    render(<TweetUsername username={username} />);
    const testUsername = screen.getByText('@' + username);
    expect(testUsername).toBeInTheDocument();
  });
});
