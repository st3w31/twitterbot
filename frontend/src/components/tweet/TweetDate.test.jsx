import { render, screen } from '@testing-library/react';
import { format } from 'date-fns';
import { toDate } from 'date-fns-tz';
import { describe, expect, test } from 'vitest';

import TweetDate from './TweetDate';

describe('TweetDatee', () => {
  const created_at = new Date(2022, 12, 8, 10, 50);
  test('show right date formatted', () => {
    render(<TweetDate created_at={created_at} />);
    const testParagraph = screen.getByText(
      format(toDate(created_at), 'yyyy-MM-dd HH:mm'),
    );
    expect(testParagraph).toBeInTheDocument();
  });
});
