import PropTypes from 'prop-types';

function TweetUsername({ username }) {
  return <p className="text-sm text-gray-500">@{username}</p>;
}

TweetUsername.propTypes = {
  username: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default TweetUsername;
