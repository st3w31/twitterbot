import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';

import TweetUserImage from './TweetUserImage';

const imageUrl = 'frontend/public/images/twitter.png';

describe('TweetUserImage', () => {
  test('src contains right path', () => {
    render(<TweetUserImage profile_image_url={imageUrl} />);
    const testImage = screen.getByRole('img');
    expect(testImage).toHaveAttribute('src', imageUrl);
  });

  test('show image when it is loaded', () => {
    render(<TweetUserImage profile_image_url={imageUrl} />);
    const testImage = screen.getByRole('img');
    expect(testImage).toBeInTheDocument();
  });
});
