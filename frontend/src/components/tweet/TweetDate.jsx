import { format } from 'date-fns';
import { toDate } from 'date-fns-tz';
import PropTypes from 'prop-types';

function TweetDate({ created_at }) {
  return (
    <p className="text-xs font-medium tracking-wide text-gray-500 antialiased">
      {format(toDate(created_at), 'yyyy-MM-dd HH:mm')}
    </p>
  );
}

TweetDate.propTypes = {
  created_at: PropTypes.string.isRequired,
};

export default TweetDate;
