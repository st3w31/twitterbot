import { render, screen } from '@testing-library/react';
import { describe, expect, it } from 'vitest';

import { twitterBaseUrl } from '@/utils/utils';

import TweetName from './TweetName';

describe('TweetName', () => {
  const username = 'elonmusk';
  const name = 'Elon Musk';

  it('should have href equal to twitterBaseUrl + uername ', () => {
    render(<TweetName username={username} name={name} />);
    const linkElement = screen.getByText(name);
    expect(linkElement).toHaveAttribute(
      'href',
      twitterBaseUrl + `/${username}`,
    );
  });
});
