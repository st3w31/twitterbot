import PropTypes from 'prop-types';

import { twitterBaseUrl } from '@/utils/utils';

function TweetName({ username, name }) {
  return (
    <a
      className="hover:underline"
      href={twitterBaseUrl + `/${username}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      {name}
    </a>
  );
}

TweetName.propTypes = {
  username: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default TweetName;
