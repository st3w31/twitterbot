import {
  ArrowPathRoundedSquareIcon,
  ChatBubbleOvalLeftIcon,
  HandThumbUpIcon,
} from '@heroicons/react/24/outline';
import { CheckBadgeIcon } from '@heroicons/react/24/solid';
import classNames from 'classnames';
import { Carousel } from 'flowbite-react';
import Linkify from 'linkify-react';
import PropTypes from 'prop-types';
import unescape from 'unescape';

import {
  mediaPropTypes,
  tweetPropTypes,
  userPropTypes,
} from '@/utils/prop-types';
import { twitterBaseUrl } from '@/utils/utils';

import TweetDate from './TweetDate';
import TweetName from './TweetName';
import TweetUserImage from './TweetUserImage';
import TweetUsername from './TweetUsername';
import TwitterLogo from './TwitterLogo';

function Tweet({ tweet, user, customProps, media }) {
  const { text, created_at, public_metrics, attachments } = tweet;
  const { name, username, profile_image_url, verified } = user;
  const container = customProps?.container ?? '';
  const showTwitterLogo = customProps?.showTwitterLogo ?? true;

  // See: https://linkify.js.org/docs/linkify-react.html
  // Section: Custom Link Components
  const renderLink = ({ attributes, content }) => {
    const { href, ...props } = attributes;

    return (
      <a
        href={href}
        className="!text-blue-500 no-underline hover:underline"
        target="_blank"
        rel="noopener noreferrer"
        {...props}
      >
        {content}
      </a>
    );
  };

  function excludeVideoAttachments(media_keys) {
    const photoAttachments = [];
    if (media_keys) {
      for (const key of media_keys) {
        if (media[key]?.type == 'photo') {
          photoAttachments.push(key);
        }
      }
      if (photoAttachments.length == 0) {
        return null;
      }
      return photoAttachments;
    } else {
      return null;
    }
  }

  function filterAttachmentsWithoutUrl(media_keys) {
    const filteredAttachmentsKey = [];
    for (const key of media_keys) {
      if (media[key]?.url) {
        filteredAttachmentsKey.push(key);
      }
    }
    return filteredAttachmentsKey;
  }

  return (
    <div className={classNames(container, 'flex w-full flex-col')}>
      <div className="flex flex-row items-center justify-between">
        <div className="flex flex-row items-center">
          <TweetUserImage profile_image_url={profile_image_url} />

          <div className="!ml-2 font-medium">
            <h3 className="flex flex-row items-center">
              <TweetName username={username} name={name} />

              {verified && (
                <span className="!ml-1">
                  <CheckBadgeIcon className="h-5 w-5 text-blue-500" />
                </span>
              )}
            </h3>
            <TweetUsername username={username} />
          </div>
        </div>

        {showTwitterLogo && <TwitterLogo />}
      </div>

      <div className="flex">
        <Linkify
          as="p"
          className="!my-2 block w-full font-medium"
          options={{
            render: renderLink,
            formatHref: {
              hashtag: (href) => twitterBaseUrl + '/hashtag/' + href.substr(1),
              mention: (href) => twitterBaseUrl + href,
            },
          }}
        >
          {unescape(text)}
        </Linkify>
      </div>
      {excludeVideoAttachments(attachments?.media_keys) ? (
        <div className="h-56 sm:h-64 xl:h-80 2xl:h-96 mb-2">
          <Carousel>
            {filterAttachmentsWithoutUrl(attachments?.media_keys).map(
              (mediaKey) => {
                const mediaUrl = media[mediaKey]?.url;
                const alt = media[mediaKey]?.alt_text;
                return <img key={mediaKey} src={mediaUrl} alt={alt} />;
              },
            )}
          </Carousel>
        </div>
      ) : (
        <></>
      )}

      <div className="flex flex-row items-center justify-between">
        <TweetDate created_at={created_at} />

        <div className="flex flex-row">
          {[
            {
              icon: (
                <ChatBubbleOvalLeftIcon className="h-4 w-4 stroke-blue-500" />
              ),
              metric: public_metrics.reply_count,
            },
            {
              icon: (
                <ArrowPathRoundedSquareIcon className="h-4 w-4 stroke-blue-500" />
              ),
              metric: public_metrics.retweet_count,
            },
            {
              icon: <HandThumbUpIcon className="h-4 w-4 stroke-blue-500" />,
              metric: public_metrics.like_count,
            },
          ].map((item, index, array) => {
            return (
              <span
                key={index}
                className={classNames(
                  index !== array.length - 1 ? 'mr-1' : '',
                  'flex flex-row items-center',
                )}
              >
                {item.icon}
                <p className="!ml-0.5 text-xs font-medium text-gray-500">
                  {item.metric}
                </p>
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}

Tweet.propTypes = {
  tweet: tweetPropTypes(),
  user: userPropTypes(),
  media: PropTypes.objectOf(mediaPropTypes()),
  customProps: PropTypes.shape({
    container: PropTypes.string,
    showTwitterLogo: PropTypes.bool,
  }),
};

export default Tweet;
