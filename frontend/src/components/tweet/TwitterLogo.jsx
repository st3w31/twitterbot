function TwitterLogo() {
  return <img className="h-5 w-5" src="/images/twitter.png" alt="" />;
}

export default TwitterLogo;
