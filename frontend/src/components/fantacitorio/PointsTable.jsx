import { TrashIcon } from '@heroicons/react/24/solid';
import PropTypes from 'prop-types';

function PointsTable({
  tableData,
  handleDelete,
  handleAddRow,
  handleNameChange,
  handlePointsChange,
}) {
  return (
    <div className="flex flex-auto flex-col mb-2">
      <div className="flex justify-start mb-2">
        <div className="w-full">
          <form onSubmit={handleAddRow} className="flex w-full flex-row">
            <button
              type="submit"
              className="relative w-1/3 inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded-l-md "
            >
              Add row
            </button>
            <div className="w-1/2">
              <input
                type="text"
                className="h-full w-full border-t border-b border-gray-300 bg-white text-sm text-gray-800 placeholder:text-gray-400 placeholder:italic focus:border-gray-300 focus:outline-none focus:ring-0"
                placeholder="Name"
                onChange={handleNameChange}
              ></input>
            </div>
            <div className="w-1/2">
              <input
                type="text"
                className="h-full w-full border border-gray-300 bg-white text-sm text-gray-800 placeholder:text-gray-400 placeholder:italic focus:border-gray-300 focus:outline-none focus:ring-0 rounded-r-md"
                placeholder="Points"
                onChange={handlePointsChange}
              ></input>
            </div>
          </form>
        </div>
      </div>
      <table className="text-center shadow-lg min-w-full">
        <thead className="bg-gray-800 border-b ">
          <tr>
            <th className="bg-gray-800 text-center text-white px-6 py-4">
              Name
            </th>
            <th className="bg-gray-800 text-center text-white px-6 py-4">
              Points
            </th>
            <th className="bg-gray-800 text-center text-white px-6 py-4">
              Remove
            </th>
          </tr>
        </thead>
        <tbody>
          {tableData &&
            Object.entries(tableData).map(([index, rowData]) => {
              const name = rowData[0];
              return (
                <tr className="bg-white border-b" key={index}>
                  {rowData.map((data) => {
                    return (
                      <td
                        key={name + data}
                        className="py-4 whitespace-nowrap text-md font-medium text-gray-900"
                      >
                        {data}
                      </td>
                    );
                  })}
                  <td className="flex items-center justify-center py-4 whitespace-nowrap ">
                    <button
                      className="inline-block px-4 py-1.5 bg-transparent hover:bg-transparent leading-tight"
                      id={name}
                      onClick={handleDelete}
                    >
                      <TrashIcon className="h-5 w-5 text-red-600 hover:text-red-500" />
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

PointsTable.propTypes = {
  tableData: PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.array.isRequired,
  ]),
  handleDelete: PropTypes.func,
  handleAddRow: PropTypes.func,
  handleNameChange: PropTypes.func,
  handlePointsChange: PropTypes.func,
};

export default PointsTable;
