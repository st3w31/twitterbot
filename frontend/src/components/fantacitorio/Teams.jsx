import useAxios from 'axios-hooks';
import { Button, Spinner } from 'flowbite-react';
import PropTypes from 'prop-types';
import { useMemo, useState } from 'react';

import Pagination from '../Pagination';
import Tweets from '../Tweets';

function Teams({ startTime, endTime }) {
  const [fantaTeams, setFantaTeams] = useState(null);
  const [searchUser, setSearchUser] = useState('');
  const fantacitorioHashtag = 'fantacitorio';
  const maxResults = 400;

  const [{ data, loading }, fetchTweets] = useAxios(
    {
      method: 'POST',
      baseURL: import.meta.env.VITE_API_BASE_URL,
      headers: { 'Content-Type': 'application/json' },
      responseType: 'json',
    },
    { manual: true },
  );

  async function fetchTeams() {
    const dateFilter = {
      startTime: startTime.toISOString(),
      endTime: endTime.toISOString(),
    };
    fetchTweets({
      url: '/tweets/by/hashtag',
      data: {
        keywords: [fantacitorioHashtag],
        ...dateFilter,
        maxResults,
      },
    }).then((response) => {
      setFantaTeams(response.data.tweets);
    });
  }

  function handleTeamsButton(event) {
    event.preventDefault(); //Not reloading all the page
    fetchTeams();
  }

  function filterTweetsWithoutAttachments(tweets) {
    const filteredAttachmentsTweets = [];
    for (const tweet of tweets) {
      if (tweet?.attachments?.media_keys) {
        const key = tweet.attachments.media_keys[0];
        if (data?.media[key]?.type == 'photo') {
          filteredAttachmentsTweets.push(tweet);
        }
      }
    }
    return filteredAttachmentsTweets;
  }

  const filteredTweets = useMemo(() => {
    //NOSONAR
    if (data?.tweets) {
      const filteredTweets = filterTweetsWithoutAttachments(data?.tweets);
      setFantaTeams(filteredTweets);
    }
  }, [data]);

  function handleSearchUser(event) {
    event.preventDefault();
    setSearchUser(event.target.value);
  }

  function removeDuplicatesByName(array, name) {
    const filtered = array.filter(
      (item) => data?.users[item?.author_id].name != name,
    );
    return filtered;
  }

  function orderByUsername(tweets) {
    const copy = removeDuplicatesByName(fantaTeams, searchUser);
    const tweetsOfUsername = [];
    for (const tweet of tweets) {
      const userId = tweet?.author_id;
      if (data?.users[userId].name == searchUser) {
        tweetsOfUsername.push(tweet);
      }
    }
    return [...tweetsOfUsername, ...copy];
  }

  function handleSearchUserSubmit(event) {
    event.preventDefault();
    const orderedTweets = orderByUsername(fantaTeams);

    setFantaTeams(orderedTweets);
  }

  return (
    <div className="flex flex-col flex-auto items-center w-1/2">
      {!fantaTeams ? (
        <div>
          {loading ? (
            <div className="flex h-full items-center justify-center">
              <Spinner size="xl" aria-label="Loading tweets..." />
            </div>
          ) : (
            <Button
              onClick={handleTeamsButton}
              className="shadow-md"
              gradientDuoTone="cyanToBlue"
            >
              Show Teams
            </Button>
          )}
        </div>
      ) : (
        <div>
          <div className="flex flex-row items-center mb-2">
            <form
              onSubmit={handleSearchUserSubmit}
              className="flex w-full justify-center flex-row"
            >
              <button
                type="submit"
                className="relative inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded-l-md "
              >
                Search user
              </button>
              <div>
                <input
                  type="text"
                  className="h-full w-full border border-gray-300 bg-white text-sm text-gray-800 placeholder:text-gray-400 placeholder:italic focus:border-gray-300 focus:outline-none focus:ring-0 rounded-r-md"
                  placeholder="Name"
                  onChange={handleSearchUser}
                ></input>
              </div>
            </form>
          </div>
          <Pagination items={fantaTeams} itemsPerPage={4} itemsAs="tweets">
            <Tweets
              users={data?.users}
              media={data?.media}
              sortType="text-desc"
              tweetCustomProps={{
                container:
                  'rounded-md bg-white p-4 shadow-[0px_0px_5px_0px] shadow-blue-500/50 transition ease-in-out hover:scale-[102.5%]',
                showTwitterLogo: true,
              }}
              containerClassName="grid grid-flow-row auto-rows-auto grid-cols-1 place-content-center place-items-center gap-3.5 xl:grid-cols-2"
            />
          </Pagination>
        </div>
      )}
    </div>
  );
}

Teams.propTypes = {
  startTime: PropTypes.object.isRequired,
  endTime: PropTypes.object.isRequired,
};

export default Teams;
