import PropTypes from 'prop-types';

import Points from './Points';
import Teams from './Teams';

function Content({ points, startTime, endTime }) {
  return (
    <div className="flex flex-row flex-wrap items-start gap-x-8">
      <Points points={points} />
      <Teams startTime={startTime} endTime={endTime} />
    </div>
  );
}

Content.propTypes = {
  startTime: PropTypes.object.isRequired,
  endTime: PropTypes.object.isRequired,
  points: PropTypes.object.isRequired,
};

export default Content;
