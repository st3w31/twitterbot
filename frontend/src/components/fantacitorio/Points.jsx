import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import PointsTable from './PointsTable';

function Points({ points }) {
  const [tableData, setTableData] = useState({});
  const [newName, setNewName] = useState('');
  const [newPoints, setNewPoints] = useState('');

  // Raccogliere i tweet che contengono la forma: punti - nome

  function matchPattern(string) {
    const regex = new RegExp(/^\d+ punti . \w+\s\w+/, 'i');

    return regex.test(string);
  }

  function splitRow(row) {
    const [points, name] = row.split('-');
    return [points.replaceAll('PUNTI', '').trim(), name.trim()];
  }

  function sortPointsDesc(array) {
    const result = array.sort(function (a, b) {
      return b[1] - a[1];
    });

    return result;
  }

  function filterTweetByPointsPattern(tweets) {
    const data = tableData;

    for (const tweet of tweets) {
      const text = tweet.text;
      const rows = text.split(/\r?\n/);

      for (const row of rows) {
        if (matchPattern(row)) {
          const [points, name] = splitRow(row);
          if (!(name in data)) {
            data[name] = parseInt(points, 10);
          } else {
            data[name] += parseInt(points, 10);
          }
        }
      }
    }
    const result = sortPointsDesc(Object.entries(data));

    return result;
  }

  function handleNameChange(event) {
    setNewName(event.target.value);
  }

  function handlePointsChange(event) {
    setNewPoints(event.target.value);
  }

  function handleDelete(event) {
    event.preventDefault();
    const nameToDelete = event.currentTarget.id;
    const data = tableData;

    const updatedTableData = data.filter((item) => item[0] != nameToDelete);
    setTableData(updatedTableData);
  }

  function handleAddRow(event) {
    event.preventDefault();
    const tableDataAsObject = Object.assign(
      ...tableData.map(([k, v]) => ({ [k]: v })),
    );

    if (!(newName in tableDataAsObject)) {
      tableDataAsObject[newName] = parseInt(newPoints, 10);
    } else {
      tableDataAsObject[newName] += parseInt(newPoints, 10);
    }
    setTableData(sortPointsDesc(Object.entries(tableDataAsObject)));
  }

  useEffect(() => {
    const data = filterTweetByPointsPattern(points?.tweets);
    setTableData(data);
  }, []);

  return (
    <>
      {tableData ? (
        <PointsTable
          tableData={tableData}
          handleDelete={handleDelete}
          handleAddRow={handleAddRow}
          handleNameChange={handleNameChange}
          handlePointsChange={handlePointsChange}
        />
      ) : (
        <></>
      )}
    </>
  );
}

Points.propTypes = {
  points: PropTypes.object.isRequired,
};
export default Points;
