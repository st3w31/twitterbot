import PropTypes from 'prop-types';
import { useMemo, useState } from 'react';
import ReactWordcloud from 'react-wordcloud';
import Sentiment from 'sentiment';

import { tweetPropTypes } from '@/utils/prop-types';

let sentiment = new Sentiment();

export function isInGarbage(word) {
  let garbage = [
    't',
    'http',
    'https',
    'e',
    'a',
    'i',
    'o',
    'il',
    'lo',
    'la',
    'il',
    'gli',
    'le',
    'ma',
    'di',
    'da',
    'in',
    'con',
    'su',
    'per',
    'tra',
    'è',
    'che',
    'al',
    'un',
    'una',
    "un'",
    'ad',
    'ne',
    'ci',
    'sui',
    'ai',
    'dei',
    'del',
    'degli',
    'alla',
    '-',
    'nei',
    'negli',
    'della',
    'nel',
    'co',
    'ha',
  ];

  let ret = false;
  if (word === null || word === '') {
    ret = true;
  } else {
    garbage.forEach((e) => {
      if (word.toLowerCase() === e.toLowerCase()) ret = true;
    });
  }

  return ret;
}

export function getSentiment(tweets) {
  // ANALISI SENTIMENTALE DEI TWEET
  let txt = '';
  let result = null;

  if (tweets != undefined) {
    tweets.forEach((e) => {
      txt += e.text + ' ';
    });
    result = sentiment.analyze(txt);
  }
  return result;
}

export function getDataBySentiment(result) {
  let termWord = [];
  const scale = 10;
  const data = [];

  // TERM BASATA SU ANALISI SENTIMENTALE (lingua inglese)
  if (result == null) {
    return;
  }

  let found;
  result.words.forEach((e) => {
    found = false;
    termWord.forEach((i) => {
      if (e === i[0]) {
        i[1] *= 2;
        found = true;
      }
    });
    if (!found)
      termWord.push([e, Math.abs(sentiment.analyze(e).score) * scale]);
  });

  termWord.forEach((e) => {
    data.push({ text: e[0], value: e[1] });
  });

  return data;
}

export function getDataByCounting(result) {
  // TERM BASATA SU CONTEGGIO RICORRENZA PAROLE
  if (result == null) {
    return;
  }

  const N = 30;
  let obj = {};

  result.tokens.forEach((e) => {
    if (!isInGarbage(e)) obj[e] = obj[e] ? ++obj[e] : 1;
  });

  let data = [];

  data = Object.keys(obj)
    .slice(0, N)
    .sort(function (a, b) {
      return obj[b] - obj[a];
    });

  data.forEach((e, i) => {
    data.splice(i, 1, { text: e, value: obj[e] });
  });

  return data;
}

/* se la sentiment ha raccolto abbastanza dati (ad esempio lingua = inglese)
 *  raccolgo dati dalla sentiment, altrimenti conto le parole e prendo le 20 più
 *  ricorrenti
 */
export function calculateWords(tweets) {
  let result = getSentiment(tweets);

  if (result != null && result.words.length > 20) {
    return getDataBySentiment(result);
  }

  return getDataByCounting(result);
}

const angles = [
  { rotations: 2, rotationAngles: [0, 0], fontSizes: [10, 40] },
  { rotations: 2, rotationAngles: [-90, 0], fontSizes: [10, 40] },
  { rotations: 2, rotationAngles: [-45, 45], fontSizes: [10, 40] },
  { rotations: 2, rotationAngles: [-30, 60], fontSizes: [10, 40] },
];

export function regulateValue(data) {
  let max = 0;

  if (data != undefined) {
    data.forEach((e) => {
      e.value = e.value + parseInt(Math.random() * 3); //NOSONAR
      if (max < e.value) max = e.value;
    });

    data.forEach((e) => {
      e.value = parseInt((e.value / max) * 10);
      e.value = e.value + parseInt(Math.random() * 3); //NOSONAR
    });
  }

  return data;
}

export default function TermCloud({ tweets }) {
  let data = useMemo(() => calculateWords(tweets), [tweets]);

  const [currentAngle, setCurrentAngle] = useState(angles[0]);

  function termAngle() {
    const nextAngle = (angles.indexOf(currentAngle) + 1) % angles.length;
    setCurrentAngle(angles[nextAngle]);
  }

  data = regulateValue(data);

  return (
    <ReactWordcloud onClick={termAngle} options={currentAngle} words={data} />
  );
}

TermCloud.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
};
