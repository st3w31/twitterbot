function Footer() {
  return (
    <footer className="container mx-auto p-4">
      <hr className="mb-4" />

      <div className="flex items-center justify-center">
        <span className="text-sm text-gray-500 sm:text-base">
          © 2022 Team#1. All Rights Reserved.
        </span>
      </div>
    </footer>
  );
}

export default Footer;
