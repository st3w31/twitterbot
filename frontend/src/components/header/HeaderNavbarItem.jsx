import classNames from 'classnames';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

function HeaderNavbarItem({ item, isMobile = false }) {
  return (
    <NavLink
      end={true}
      to={item.href}
      className={({ isActive }) =>
        classNames(
          isActive
            ? 'bg-sky-500 text-white shadow-md '
            : 'text-gray-200 hover:bg-sky-500 hover:text-white',
          isMobile ? 'block text-base' : 'text-sm',
          'rounded-md px-3 py-2 font-medium',
        )
      }
    >
      {item.name}
    </NavLink>
  );
}

HeaderNavbarItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
  }).isRequired,
  isMobile: PropTypes.bool,
};

export default HeaderNavbarItem;
