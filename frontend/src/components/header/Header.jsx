import { Disclosure } from '@headlessui/react';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline';

import { useAuth } from '@/hooks/useAuth';

import HeaderNavbarItem from './HeaderNavbarItem';

const navigation = [
  { name: 'Home', href: '.' },
  { name: "L'eredità", href: './eredita' },
  { name: 'Fantacitorio', href: './fantacitorio' },
  { name: 'Chess', href: '/chess' },
];

function Header() {
  const { isAuthenticated, login, logout } = useAuth();

  return (
    <Disclosure as="nav" className="bg-sky-700 ">
      {({ open }) => (
        <>
          <div className="container mx-auto px-4">
            <div className="relative flex h-16 items-center justify-between">
              <a
                href="/"
                className="text-xl font-bold text-white transition duration-300 hover:text-slate-300 hover:ease-in-out"
              >
                {import.meta.env.VITE_APP_NAME}
              </a>

              <div className="flex items-center sm:hidden">
                <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-white hover:bg-sky-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                  <span className="sr-only">Open main menu</span>
                  {open ? (
                    <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                  ) : (
                    <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>

              <div className="hidden sm:block">
                <div className="flex space-x-4">
                  {navigation.map((item) => (
                    <HeaderNavbarItem key={item.name} item={item} />
                  ))}

                  {!isAuthenticated ? (
                    <button
                      className="text-white hover:text-slate-300"
                      onClick={login}
                    >
                      Login
                    </button>
                  ) : (
                    <button className="text-white" onClick={logout}>
                      Logout
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>

          <Disclosure.Panel className="sm:hidden">
            <div className="space-y-1 px-4 pb-4 text-center">
              {navigation.map((item) => (
                <HeaderNavbarItem key={item.name} item={item} isMobile={true} />
              ))}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}

export default Header;
