import { describe, expect, expectTypeOf, it } from 'vitest';

import { analyzeTweets } from './Sentiment.jsx';

describe('analyze tweets', () => {
  let tweet = [{ text: 'test' }];
  let goodTweet = [{ text: 'good' }];
  let badTweet = [{ text: 'bad' }];

  it('should return an array of object', () => {
    let result = analyzeTweets(tweet);
    expectTypeOf(result).toBeArray();
    expectTypeOf(result).items.toEqualTypeOf(Object);
  });

  it('should go in score > 0', () => {
    let result = analyzeTweets(goodTweet);
    expect(result[0].value).toBe(1);
  });

  it('should go in score < 0', () => {
    let result = analyzeTweets(badTweet);
    expect(result[1].value).toBe(1);
  });

  it('should go in else', () => {
    let result = analyzeTweets(tweet);
    expect(result[2].value).toBe(1);
  });
});
