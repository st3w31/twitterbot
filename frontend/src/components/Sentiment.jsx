import PropTypes from 'prop-types';
import { useMemo } from 'react';
import {
  Cell,
  Legend,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip,
} from 'recharts';
import ReactSentiment from 'sentiment';

import { tweetPropTypes } from '@/utils/prop-types';

const mysentiment = new ReactSentiment();

export const analyzeTweets = (tweets) => {
  let data = [
    { name: 'Positive tweets', value: 0 },
    { name: 'Negative tweets', value: 0 },
    { name: 'Neutral tweets', value: 0 },
  ];

  tweets.forEach((element) => {
    let score = mysentiment.analyze(element.text).score;
    if (score > 0) {
      data[0].value++;
    } else if (score < 0) {
      data[1].value++;
    } else {
      data[2].value++;
    }
  });

  return data;
};

function Sentiment({ tweets }) {
  const renderChart = useMemo(() => {
    return (
      <ResponsiveContainer
        data-testid="react-test-sentiment"
        width="100%"
        height="100%"
      >
        <PieChart>
          <Legend verticalAlign="top" height={36} />
          <Tooltip />

          <Pie
            data={analyzeTweets(tweets)}
            dataKey="value"
            nameKey="name"
            cx="50%"
            cy="50%"
            outerRadius={125}
          >
            <Cell fill="#276600" />
            <Cell fill="#b30000" />
            <Cell fill="#808080" />
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    );
  }, [tweets]);

  return renderChart;
}

Sentiment.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
};

export default Sentiment;
