import PropTypes from 'prop-types';
import { useMemo } from 'react';

import {
  mediaPropTypes,
  tweetPropTypes,
  userPropTypes,
} from '@/utils/prop-types';

import Tweet from './tweet/Tweet';

function Tweets({
  tweets,
  users,
  media,
  tweetCustomProps,
  containerClassName,
  sortType,
}) {
  /**
   * Function to sort tweets according to the sortType.
   * The sort type is selected with the toggle
   *
   * @param {Array} tweets
   * @returns tweets order accordint to the sort type.
   */
  function sort(tweets) {
    let sortBy = sortByDateDesc;
    const tweetsCopy = [...tweets];

    switch (sortType) {
      case 'date-asc':
        sortBy = sortByDateAsc;
        break;

      case 'text-desc':
        sortBy = sortByTextDesc;
        break;
    }

    return tweetsCopy.sort(sortBy);
  }

  /**
   * Comparator for sorting tweets by date (ascending).
   *
   * @param {Object} a Tweet.
   * @param {Object} b Tweet.
   * @returns difference between "a.created_at" and "b.created_at".
   */
  function sortByDateAsc(a, b) {
    const dateA = new Date(a.created_at).getTime();
    const dateB = new Date(b.created_at).getTime();
    return dateA - dateB;
  }

  /**
   * Comparator for sorting tweets by date (descending).
   *
   * @param {Object} a Tweet.
   * @param {Object} b Tweet.
   * @returns difference between "b.created_at" and "a.created_at".
   */
  function sortByDateDesc(a, b) {
    const dateA = new Date(a.created_at).getTime();
    const dateB = new Date(b.created_at).getTime();
    return dateB - dateA;
  }

  /**
   * Comparator for sorting tweets by text length (descending).
   *
   * @param {Object} a Tweet.
   * @param {Object} b Tweet.
   * @returns difference between "b.text.length" and "a.text.length".
   */
  function sortByTextDesc(a, b) {
    return b.text.length - a.text.length;
  }

  const renderTweets = useMemo(() => {
    if (!tweets || !users) {
      return null;
    }

    return (
      <div className={containerClassName}>
        {sort(tweets).map((tweet) => {
          const user = users[tweet.author_id];
          return (
            <Tweet
              key={tweet.id}
              tweet={tweet}
              media={media}
              user={user}
              customProps={tweetCustomProps}
            />
          );
        })}
      </div>
    );
  }, [tweets, users]);

  return renderTweets;
}

Tweets.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
  users: PropTypes.objectOf(userPropTypes({ id: false })),
  media: PropTypes.objectOf(mediaPropTypes()),
  tweetCustomProps: PropTypes.object,
  containerClassName: PropTypes.string,
  sortType: PropTypes.string,
};

export default Tweets;
