import { describe, expect, test } from 'vitest';

import {
  calculateWords,
  getDataByCounting,
  getDataBySentiment,
  getSentiment,
  isInGarbage,
} from './TermCloud';

describe('isInGarbage', () => {
  test('should return false for falsewords, true for trueWords', () => {
    const falseWord = ['tweet', 'htttp', 'false1', 'false2', 'false3'];
    const trueWord = [
      't',
      'http',
      'https',
      'e',
      'a',
      'i',
      'o',
      'il',
      'lo',
      'la',
      'il',
      'gli',
      'le',
      'ma',
      'di',
      'da',
      'in',
      'con',
      'su',
      'per',
      'tra',
      'è',
      'che',
      'al',
      'un',
      'una',
      "un'",
      'ad',
      'ne',
      'ci',
      'sui',
      'ai',
      'dei',
      'del',
      'degli',
      'alla',
      '-',
      'nei',
      'negli',
      'della',
      'nel',
      'co',
      'ha',
    ];

    falseWord.forEach((e) => {
      expect(isInGarbage(e)).toBe(false);
    });
    trueWord.forEach((e) => {
      expect(isInGarbage(e)).toBe(true);
    });
  });

  test('null element', () => {
    expect(isInGarbage(null)).toBe(true);
  });

  test('empty string element', () => {
    expect(isInGarbage('')).toBe(true);
  });
});

describe('getSentiment', () => {
  test('null element', () => {
    expect(getSentiment(null)).toBe(null);
  });
});

describe('getDataBySentiment', () => {
  test('null element', () => {
    expect(getDataBySentiment(null)).toBe(undefined);
  });
  test('undefined element', () => {
    expect(getDataBySentiment(undefined)).toBe(undefined);
  });
});

describe('getDataByCounting', () => {
  test('null element', () => {
    expect(getDataByCounting(null)).toBe(undefined);
  });
  test('undefined element', () => {
    expect(getDataByCounting(undefined)).toBe(undefined);
  });
});

describe('calculateWords', () => {
  test('null element', () => {
    expect(calculateWords(null)).toBe(undefined);
  });
});
