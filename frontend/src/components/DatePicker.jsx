import {
  CalendarDaysIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/24/solid';
import {
  endOfDay,
  format,
  getMonth,
  getYear,
  subDays,
  subSeconds,
} from 'date-fns';
import { range } from 'lodash';
import allMonths from 'months';
import PropTypes from 'prop-types';
import { useEffect, useMemo, useState } from 'react';
import ReactDatePicker from 'react-datepicker';

import './DatePicker.css';

const styles = {
  select:
    'block w-full border border-gray-300 bg-gray-50 p-2 text-sm text-gray-900 focus:border-gray-300 focus:outline-none focus:ring-0',
};

function DatePicker({ handleDatePicker, customProp }) {
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);

  const years = range(2006, getYear(new Date()) + 1, 1);
  const months = allMonths;

  useEffect(() => {
    const [startTime, endTime] = defaultDateFilter();
    setStartTime(startTime);
    setEndTime(endTime);
  }, []);

  /**
   * This function calls handleDatePicker whenever startTime and endTime
   * are changed. This way we keep the "startTime" and "endTime" in the
   * Home synchronized.
   */
  useEffect(() => {
    handleDatePicker(startTime, endTime);
  }, [startTime, endTime]);

  /**
   * On change event.
   *
   * @param {[Date, Date]} dates Dates.
   */
  function handleRangeInput(dates) {
    const [start, end] = dates;

    if (start instanceof Date) {
      setStartTime(start);

      // Problem: start equals to end.
      // We have this case when the user selects the same day as the "start"
      // and "end" date. To avoid an error we have to set "endTime" at the
      // end of the day selected.
      if (end instanceof Date && start.getTime() === end.getTime()) {
        const endIsToday =
          format(end, 'yyyy-MM-dd') === format(new Date(), 'yyyy-MM-dd');

        if (endIsToday) {
          const nowMinusTenSeconds = subSeconds(new Date(), 10);
          return setEndTime(nowMinusTenSeconds);
        }

        return setEndTime(endOfDay(end));
      }

      setEndTime(end);
    }
  }

  /**
   * Default dates.
   *
   * @returns {[Date, Date]} Default dates.
   */
  function defaultDateFilter() {
    const endTime = subSeconds(new Date(), 10);
    const startTime = subDays(endTime, '7');
    return [startTime, endTime];
  }

  const renderDatePicker = useMemo(() => {
    return (
      <ReactDatePicker
        calendarClassName="border-gray-300 rounded-md"
        renderCustomHeader={({
          date,
          changeYear,
          changeMonth,
          decreaseMonth,
          increaseMonth,
          prevMonthButtonDisabled,
          nextMonthButtonDisabled,
        }) => (
          <div className="m-2 flex flex-row justify-center">
            <button
              className="mr-1"
              onClick={decreaseMonth}
              disabled={prevMonthButtonDisabled}
            >
              <ChevronLeftIcon className="h-5 w-5 text-gray-400" />
            </button>

            <select
              className={`rounded-l-md ${styles.select}`}
              value={getYear(date)}
              onChange={({ target: { value } }) => changeYear(value)}
            >
              {years.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>

            <select
              className={`rounded-r-md border-l-0 ${styles.select}`}
              value={months[getMonth(date)]}
              onChange={({ target: { value } }) =>
                changeMonth(months.indexOf(value))
              }
            >
              {months.map((option) => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>

            <button
              className="ml-1"
              onClick={increaseMonth}
              disabled={nextMonthButtonDisabled}
            >
              <ChevronRightIcon className="h-5 w-5 text-gray-400" />
            </button>
          </div>
        )}
        customInput={
          <button type="button" className={customProp}>
            <CalendarDaysIcon className="h-6 w-6" />
          </button>
        }
        onChange={handleRangeInput}
        startDate={startTime}
        endDate={endTime}
        minDate={new Date('2006-03-21')}
        maxDate={new Date()}
        selectsRange={true}
        adjustDateOnChange={true}
        withPortal
      />
    );
  }, [startTime, endTime]);

  return renderDatePicker;
}

DatePicker.propTypes = {
  handleDatePicker: PropTypes.func.isRequired,
  customProp: PropTypes.string.isRequired,
};

export default DatePicker;
