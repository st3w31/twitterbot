import PropTypes from 'prop-types';
import { useMemo } from 'react';

function Slider({ handleRangeInput, maxResults }) {
  const renderSlider = useMemo(() => {
    return (
      <>
        <label
          htmlFor="steps-range"
          className="block mb-2 text-sm font-medium text-gray-900"
        >
          Max results: {maxResults}
        </label>

        <input
          id="steps-range"
          type="range"
          min="10"
          max="500"
          step="1"
          onInput={handleRangeInput}
          value={maxResults}
          className="w-1/2 justify-center h-2 bg-gray-200 rounded-lg appearance-none cursor-pointer"
        />
      </>
    );
  }, [maxResults]);

  return renderSlider;
}

Slider.propTypes = {
  handleRangeInput: PropTypes.func.isRequired,
  maxResults: PropTypes.number.isRequired,
};

export default Slider;
