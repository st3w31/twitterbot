import PropTypes from 'prop-types';
import { useMemo } from 'react';
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';

import { tweetPropTypes } from '@/utils/prop-types';

function Chart({ tweets }) {
  function serialize(tweets) {
    const o = {};

    for (const tweet of tweets) {
      const date = tweet.created_at.split('T')[0];
      const { like_count, reply_count, retweet_count } = tweet.public_metrics;

      if (!(date in o)) {
        o[date] = { tweets: 0, likes: 0, replies: 0, retweets: 0 };
      }

      o[date].tweets++;
      o[date].likes += like_count;
      o[date].replies += reply_count;
      o[date].retweets += retweet_count;
    }

    return [
      ...Object.entries(o).map((entry) => {
        return { date: entry[0], ...entry[1] };
      }),
    ].reverse();
  }

  const renderChart = useMemo(() => {
    return (
      <ResponsiveContainer width="100%" height="100%">
        <LineChart data={serialize(tweets)}>
          <XAxis dataKey="date" />
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip />
          <Legend verticalAlign="top" height={72} />
          <Line
            name="number of tweets"
            type="monotone"
            dataKey="tweets"
            stroke="#8884d8"
          />
          <Line
            name="number of likes"
            type="monotone"
            dataKey="likes"
            stroke="#823344"
          />
          <Line
            name="number of retweets"
            type="monotone"
            dataKey="retweets"
            stroke="#82ca9d"
          />
          <Line
            name="number of replies"
            type="monotone"
            dataKey="replies"
            stroke="#276600"
          />
        </LineChart>
      </ResponsiveContainer>
    );
  }, [tweets]);

  return renderChart;
}

Chart.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
};

export default Chart;
