import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/24/solid';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Children, cloneElement, useState } from 'react';
import ReactPaginate from 'react-paginate';

const styles = {
  pageUnorderedList: 'flex w-min mx-auto mt-2 shadow rounded-md',
  pageListItem: 'flex bg-white border border-gray-300 self-stretch',
  pageAnchor: 'flex items-center p-3 text-gray-500',
};

function Pagination({ items, itemsPerPage, children, itemsAs }) {
  const [currentOffset, setCurrentOffset] = useState(0);

  const nextOffset = currentOffset + itemsPerPage;
  const totalPages = Math.ceil(items.length / itemsPerPage);
  const currentItems = items.slice(currentOffset, nextOffset);

  /**
   * Handle on change (react-paginate).
   *
   * @param {number} selected Selected item.
   */
  function handleOnChange({ selected }) {
    const newOffset = (selected * itemsPerPage) % items.length;
    setCurrentOffset(newOffset);
  }

  return (
    <>
      {Children.map(children, (child) => {
        const as = itemsAs ?? 'currentItems';
        return cloneElement(child, { [as]: currentItems }, null);
      })}

      {totalPages > 1 && (
        <ReactPaginate
          onPageChange={handleOnChange}
          pageCount={totalPages}
          marginPagesDisplayed={1}
          previousLabel={<ChevronLeftIcon className="h-5 w-5" />}
          nextLabel={<ChevronRightIcon className="h-5 w-5" />}
          containerClassName={styles.pageUnorderedList}
          breakClassName={classNames(styles.pageListItem, 'border-r-0')}
          breakLinkClassName={styles.pageAnchor}
          pageClassName={classNames(styles.pageListItem, 'border-r-0')}
          previousClassName={classNames(
            styles.pageListItem,
            'border-r-0 rounded-l-md',
          )}
          nextClassName={classNames(styles.pageListItem, 'rounded-r-md')}
          pageLinkClassName={styles.pageAnchor}
          previousLinkClassName={styles.pageAnchor}
          nextLinkClassName={styles.pageAnchor}
          activeLinkClassName={classNames(
            styles.pageAnchor,
            'font-bold bg-gray-100',
          )}
        />
      )}
    </>
  );
}

Pagination.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired,
  itemsAs: PropTypes.string,
};

export default Pagination;
