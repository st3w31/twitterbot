import { ToggleSwitch } from 'flowbite-react';
import PropTypes from 'prop-types';
import { useEffect, useMemo, useState } from 'react';

function SortFilters({ handleSortType }) {
  const [sortTypes, setSortTypes] = useState({
    'date-asc': false,
    'text-desc': true,
  });

  useEffect(() => {
    handleSortType('text-desc');
  }, []);

  /**
   * Handle on click.
   *
   * @param {string} type Sort type.
   */
  function handleOnClick(type) {
    const newSortTypes = { ...sortTypes };

    // Sets all sort types to false (except for "type")
    for (const sortType in newSortTypes) {
      if (sortType !== type) {
        newSortTypes[sortType] = false;
      }
    }

    newSortTypes[type] = !newSortTypes[type];

    // We must call "handleSortType" only if the key "type" is set to true.
    // (in others words only when the toggle "type" is checked)
    if (newSortTypes[type]) {
      handleSortType(type);
    } else {
      handleSortType(null);
    }

    setSortTypes(newSortTypes);
  }

  const renderToggleSwitches = useMemo(() => {
    return (
      <div className="flex justify-center gap-x-5">
        <ToggleSwitch
          color="info"
          label="Sort by date (asc)"
          checked={sortTypes['date-asc']}
          onClick={() => handleOnClick('date-asc')}
        />

        <ToggleSwitch
          color="info"
          label="Sort by text length (desc)"
          checked={sortTypes['text-desc']}
          onClick={() => handleOnClick('text-desc')}
        />
      </div>
    );
  }, [sortTypes]);

  return renderToggleSwitches;
}

SortFilters.propTypes = {
  handleSortType: PropTypes.func.isRequired,
};

export default SortFilters;
