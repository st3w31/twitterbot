import { ExclamationCircleIcon } from '@heroicons/react/24/solid';
import { Alert } from 'flowbite-react';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

function Error({ error }) {
  const [dataError, setDataError] = useState(null);

  useEffect(() => {
    setDataError(error?.response?.data);
  }, [error]);

  /**
   * Render validation errors.
   */
  function renderValidationErrors() {
    if (!dataError?.errors) {
      return null;
    }

    return (
      <ul className="list-disc list-inside">
        {Object.keys(dataError.errors).map((key, index) => {
          const message = dataError.errors[key].msg;
          return <li key={index}>{message}</li>;
        })}
      </ul>
    );
  }

  /**
   * Render default error.
   */
  function renderDefaultError() {
    return dataError?.message && <span>{dataError.message}</span>;
  }

  return (
    dataError && (
      <Alert
        className="col-span-2"
        color="failure"
        withBorderAccent={true}
        icon={ExclamationCircleIcon}
      >
        {renderValidationErrors() || renderDefaultError()}
      </Alert>
    )
  );
}

Error.propTypes = {
  error: PropTypes.object,
};

export default Error;
