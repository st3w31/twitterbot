import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';

import SubmitButton from './SubmitButton';

describe('SubmitButton', () => {
  test('The rendering of button click', () => {
    render(<SubmitButton />);
    const testButton = screen.getByRole('button');

    expect(testButton).toBeInTheDocument();
    expect(testButton).toHaveAttribute('type', 'submit');
  });
});
