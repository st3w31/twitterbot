import { Listbox, Transition } from '@headlessui/react';
import {
  HashtagIcon,
  MagnifyingGlassIcon,
  MapPinIcon,
  UserCircleIcon,
} from '@heroicons/react/24/solid';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';

import DatePicker from '../DatePicker';
import SearchInput from './SearchInput';

const searchTypes = [
  {
    name: 'username',
    icon: <UserCircleIcon className="h-6 w-6 text-gray-400" />,
    placeholder: 'Search by username...',
  },
  {
    name: 'location',
    icon: <MapPinIcon className="h-6 w-6 text-gray-400" />,
    placeholder: 'Search by location...',
  },
  {
    name: 'hashtag',
    icon: <HashtagIcon className="h-6 w-6 text-gray-400" />,
    placeholder: 'Search by hashtag...',
  },
];

function SearchBar({
  handleFormSubmit,
  handleSearchInput,
  handleSearchType,
  handleDatePicker,
}) {
  const [type, setType] = useState(searchTypes[0]);

  useEffect(() => {
    handleSearchType(searchTypes[0].name);
  }, []);

  return (
    <div className="flex flex-row rounded-l-2xl rounded-r-2xl">
      <DatePicker
        handleDatePicker={handleDatePicker}
        customProp={
          'relative h-full rounded-l-2xl border border-r-0 border-gray-300 bg-gray-100 px-4 text-gray-400 hover:bg-gray-200'
        }
      />

      <form onSubmit={handleFormSubmit} className="flex w-full flex-row">
        <div className="relative">
          <Listbox
            onChange={(event) => {
              setType(event);
              handleSearchType(event.name);
            }}
          >
            <Listbox.Button className="h-full border border-gray-300 bg-gray-100 px-4 hover:bg-gray-200">
              {type.icon}
            </Listbox.Button>

            <Transition
              as={Fragment}
              leave="transition ease-in-out duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options className="top-100 absolute z-[999999] w-full divide-y divide-gray-300 rounded-b border border-t-0 border-gray-300 bg-gray-100">
                {searchTypes.map((searchType, index) => {
                  if (searchType.name === type.name) {
                    return;
                  }

                  return (
                    <Listbox.Option
                      key={index}
                      className={({ active }) => {
                        return classNames(
                          'flex justify-center p-3',
                          active ? 'bg-gray-300' : '',
                        );
                      }}
                      value={searchType}
                    >
                      {searchType.icon}
                    </Listbox.Option>
                  );
                })}
              </Listbox.Options>
            </Transition>
          </Listbox>
        </div>

        <div className="w-full">
          <SearchInput
            placeholder={type.placeholder}
            handleSearchInput={handleSearchInput}
          />
        </div>

        <div className="relative">
          <button
            type="submit"
            title="Submit"
            className="h-full rounded-r-2xl border border-blue-600 bg-blue-600 p-2.5 text-white hover:border-blue-700 hover:bg-blue-700"
          >
            <MagnifyingGlassIcon className="h-6 w-6" />
          </button>
        </div>
      </form>
    </div>
  );
}

SearchBar.propTypes = {
  handleFormSubmit: PropTypes.func.isRequired,
  handleSearchInput: PropTypes.func.isRequired,
  handleSearchType: PropTypes.func.isRequired,
  handleDatePicker: PropTypes.func.isRequired,
};

export default SearchBar;
