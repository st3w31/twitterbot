import PropTypes from 'prop-types';

function SearchInput({ placeholder, handleSearchInput }) {
  return (
    <input
      type="search"
      className="h-full w-full border border-l-0 border-r-0 border-gray-300 bg-white text-sm text-gray-800 placeholder:text-gray-400 focus:border-gray-300 focus:outline-none focus:ring-0"
      placeholder={placeholder}
      onChange={handleSearchInput}
      required
    />
  );
}

SearchInput.propTypes = {
  placeholder: PropTypes.string.isRequired,
  handleSearchInput: PropTypes.func.isRequired,
};

export default SearchInput;
