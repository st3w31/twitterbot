import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { afterEach, describe, expect, test, vi } from 'vitest';

import SearchInput from './SearchInput';

describe('SearchInput', () => {
  afterEach(() => {
    vi.restoreAllMocks();
  });

  test('The input field and his props', () => {
    const mockHandleSearchInput = vi.fn();
    const placeholder = 'testPlaceholder';

    render(
      <SearchInput
        handleSearchInput={mockHandleSearchInput}
        placeholder={placeholder}
      />,
    );
    const testSearchInput = screen.getByRole('searchbox');

    expect(testSearchInput).toBeInTheDocument();
    expect(testSearchInput).toBeTruthy();

    expect(testSearchInput?.textContent).toBe('');
    expect(testSearchInput).toHaveAttribute('type', 'search');
    expect(testSearchInput).toHaveAttribute('placeholder', placeholder);

    if (testSearchInput) {
      expect(testSearchInput).toBeRequired();
    }
  });

  test('The input from a user', async () => {
    const mockHandleSearchInput = vi.fn();
    const placeholder = 'testPlaceholder';

    const user = userEvent.setup();
    render(
      <SearchInput
        handleSearchInput={mockHandleSearchInput}
        placeholder={placeholder}
      />,
    );
    const testSearchInput = screen.getByRole('searchbox');
    await user.type(testSearchInput, 'ElonMusk');
    expect(testSearchInput).toHaveValue('ElonMusk');
    expect(mockHandleSearchInput).toHaveBeenCalled();
  });
});
