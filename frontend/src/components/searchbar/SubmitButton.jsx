import { MagnifyingGlassIcon } from '@heroicons/react/24/solid';

function SubmitButton() {
  return (
    <button
      type="submit"
      className="h-full rounded-r-2xl border border-blue-600 bg-blue-600 p-2.5 text-white hover:border-blue-700 hover:bg-blue-700"
    >
      <MagnifyingGlassIcon className="h-6 w-6" />
    </button>
  );
}

export default SubmitButton;
