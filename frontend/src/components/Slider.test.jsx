import { render, screen } from '@testing-library/react';
import { afterEach, describe, expect, test, vi } from 'vitest';

import Slider from './Slider';

describe('Move slider changes number of tweets represented', () => {
  afterEach(() => {
    vi.restoreAllMocks();
  });

  test('Move slider changes number of tweets represented', async () => {
    const mockHandleRangeInput = vi.fn();
    const maxResults = '10';

    render(
      <Slider
        handleRangeInput={mockHandleRangeInput}
        maxResults={maxResults}
      />,
    );
    const testSlider = screen.getByRole('slider');

    expect(testSlider).toHaveAttribute('type', 'range');
    expect(testSlider).toHaveAttribute('value', '10');
  });
});
