import PropTypes from 'prop-types';
import { useMemo } from 'react';
import { useMap } from 'react-leaflet';

import { placePropTypes, tweetPropTypes } from '@/utils/prop-types';

function RecenterAutomatically({ tweets, places }) {
  const map = useMap();

  /**
   * Get largest bounding box.
   *
   * @returns {Array.<Number>} Largest bounding box.
   * @returns Bounding box.
   */
  function getLargestBoundingBox() {
    const minWestLong = Math.min(
      ...tweets.map((tweet) => {
        if (isNaN(places[tweet?.geo?.place_id]?.geo?.bbox[0])) {
          return Infinity;
        }

        return places[tweet?.geo?.place_id]?.geo?.bbox[0];
      }),
    );

    const minSouthLat = Math.min(
      ...tweets.map((tweet) => {
        if (isNaN(places[tweet?.geo?.place_id]?.geo?.bbox[1])) {
          return Infinity;
        }

        return places[tweet?.geo?.place_id]?.geo?.bbox[1];
      }),
    );

    const maxEastLong = Math.max(
      ...tweets.map((tweet) => {
        if (isNaN(places[tweet?.geo?.place_id]?.geo?.bbox[2])) {
          return -Infinity;
        }

        return places[tweet?.geo?.place_id]?.geo?.bbox[2];
      }),
    );

    const maxNorthLat = Math.max(
      ...tweets.map((tweet) => {
        if (isNaN(places[tweet?.geo?.place_id]?.geo?.bbox[3])) {
          return -Infinity;
        }

        return places[tweet?.geo?.place_id]?.geo?.bbox[3];
      }),
    );

    return [
      [minSouthLat, minWestLong],
      [maxNorthLat, maxEastLong],
    ];
  }

  useMemo(() => {
    map.fitBounds(getLargestBoundingBox());
  }, [tweets, places]);

  return null;
}

RecenterAutomatically.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
  places: PropTypes.objectOf(placePropTypes({ id: false })),
};

export default RecenterAutomatically;
