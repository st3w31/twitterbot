import PropTypes from 'prop-types';
import randomColor from 'randomcolor';
import { useMemo, useState } from 'react';
import { Marker, Popup, Rectangle, useMap } from 'react-leaflet';

import Pagination from '@/components/Pagination';
import Tweets from '@/components/Tweets';
import {
  mediaPropTypes,
  placePropTypes,
  tweetPropTypes,
  userPropTypes,
} from '@/utils/prop-types';

function BoundingBox({ tweets, users, places, media }) {
  const map = useMap();
  const tweetsBoundingBox = places[tweets[0]?.geo?.place_id].geo.bbox;
  const [bounds, setBounds] = useState(sortBoundingBox(tweetsBoundingBox));

  const zoomIn = useMemo(
    () => ({
      click() {
        setBounds(bounds); //NOSONAR
        map.fitBounds(bounds);
      },
    }),
    [map],
  );

  /**
   * Sort bounding box as react-leaflet convention.
   *
   * @param {Array.<Number>} bbox Bounding box.
   * @returns {Array.<Number>} Sorted bounding box.
   */
  function sortBoundingBox(bbox) {
    const [westLong, southLat, eastLong, northLat] = bbox;

    return [
      [southLat, westLong],
      [northLat, eastLong],
    ];
  }

  /**
   * Check if a given bounding box is a marker.
   *
   * @param {Array.<Number>} bbox Bounding box.
   * @returns true
   */
  function isMarker(bbox) {
    const [westLong, southLat, eastLong, northLat] = bbox;
    return southLat === northLat && westLong === eastLong;
  }

  function renderPopup() {
    return (
      <Popup>
        <Pagination items={tweets} itemsPerPage={1} itemsAs="tweets">
          <Tweets
            users={users}
            media={media}
            tweetCustomProps={{ showTwitterLogo: false }}
          />
        </Pagination>
      </Popup>
    );
  }

  return isMarker(tweetsBoundingBox) ? (
    <Marker position={[tweetsBoundingBox[1], tweetsBoundingBox[0]]}>
      {renderPopup()}
    </Marker>
  ) : (
    <Rectangle
      bounds={bounds}
      eventHandlers={zoomIn}
      pathOptions={{ color: randomColor({ luminosity: 'dark' }) }}
    >
      {renderPopup()}
    </Rectangle>
  );
}

BoundingBox.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
  places: PropTypes.objectOf(placePropTypes({ id: false })),
  users: PropTypes.objectOf(userPropTypes({ id: false })),
  media: PropTypes.objectOf(mediaPropTypes()),
};

export default BoundingBox;
