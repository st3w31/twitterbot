import PropTypes from 'prop-types';
import { useMemo, useState } from 'react';
import { MapContainer, TileLayer } from 'react-leaflet';

import {
  mediaPropTypes,
  placePropTypes,
  tweetPropTypes,
  userPropTypes,
} from '@/utils/prop-types';

import BoundingBox from './BoundingBox';
import RecenterAutomatically from './RecenterAutomatically';

function Map({ tweets, places, users, media }) {
  // See the following links:
  // - https://gist.github.com/graydon/11198540
  // - https://gist.github.com/graydon/11198540?permalink_comment_id=2216842#gistcomment-2216842
  // Format: [[south_lat, west_long], [north_lat, east_long]]
  const [mapBounds] = useState([
    [36.619987291, 6.7499552751],
    [47.1153931748, 18.4802470232],
  ]);

  /**
   * Calculate the area of a bounding box.
   *
   * @param {Array.<Number>} bbox Bounding box.
   * @returns {Number} Area.
   */
  function calculateArea(bbox) {
    const diffWestEastLong = Math.abs(bbox[0] - bbox[2]); // x
    const diffSouthNorthLat = Math.abs(bbox[1] - bbox[3]); // y
    return diffWestEastLong * diffSouthNorthLat;
  }

  /**
   * Sort tweets by area (descending order).
   *
   * Reason: tweets with a larger area must necessarily be rendered first
   * otherwise their overlay overwrites the overlay of a tweet with a
   * smaller area thus making its interact impossible.
   *
   * @param {Array.<Object>} tweets Tweets.
   * @returns {Array.<Object>} Tweets.
   */
  function sortByArea(tweets) {
    const tweetsWithPlaceId = [];
    const tweetsWithoutPlaceId = [];

    // Separate tweets that contain the "place_id" key from those that don't.
    for (const tweet of tweets) {
      if (!tweet?.geo?.place_id) {
        tweetsWithoutPlaceId.push(tweet);
      } else {
        tweetsWithPlaceId.push(tweet);
      }
    }

    // Sort geolocated tweets by area (in place).
    tweetsWithPlaceId.sort((a, b) => {
      const placeA = places[a?.geo?.place_id].geo.bbox;
      const placeB = places[b?.geo?.place_id].geo.bbox;
      return calculateArea(placeB) - calculateArea(placeA);
    });

    return [...tweetsWithPlaceId, ...tweetsWithoutPlaceId];
  }

  /**
   * Create an array that contains arrays of tweets, grouped by bounding box.
   *
   * Reason: tweets with the same bounding box overlap and only one the last
   * one can be accessed. To avoid this, tweets with the same bounding box
   * are grouped into arrays on which pagination is applied.
   *
   * @param {Array.<Object>} tweets Tweets.
   * @returns {Array.<Object>} Tweets.
   */
  function groupByBoundingBox(tweets) {
    const data = {};

    for (const tweet of tweets) {
      const bbox = places[tweet?.geo?.place_id]?.geo?.bbox.toString();

      if (!bbox) {
        continue;
      }

      if (!(bbox in data)) {
        data[bbox] = [];
      }

      data[bbox].push(tweet);
    }

    return Object.entries(data);
  }

  const renderBoundingBoxes = useMemo(() => {
    if (!tweets || !users || !places) {
      return;
    }

    return groupByBoundingBox(sortByArea(tweets)).map(([bbox, tweets]) => {
      return (
        <BoundingBox
          key={bbox}
          tweets={tweets}
          users={users}
          places={places}
          media={media}
        />
      );
    });
  }, [tweets, users, places]);

  return (
    <MapContainer className="h-full rounded-md" zoom={50} bounds={mapBounds}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      />

      {renderBoundingBoxes}

      {places && tweets && (
        <RecenterAutomatically tweets={tweets} places={places} />
      )}
    </MapContainer>
  );
}

Map.propTypes = {
  tweets: PropTypes.arrayOf(tweetPropTypes()),
  places: PropTypes.objectOf(placePropTypes({ id: false })),
  users: PropTypes.objectOf(userPropTypes({ id: false })),
  media: PropTypes.objectOf(mediaPropTypes()),
};

export default Map;
