import useAxios from 'axios-hooks';
import PropTypes from 'prop-types';
import { useEffect, useMemo, useState } from 'react';

import { AuthContext } from './AuthContext';

export function AuthProvider({ children }) {
  const [error, setError] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const [{ data }, fetch] = useAxios(
    {
      method: 'GET',
      baseURL: import.meta.env.VITE_API_BASE_URL,
      responseType: 'json',
      // Makes the browser include cookies and authentication headers.
      // Note: Do NOT change for any reason.
      withCredentials: true,
    },
    { manual: true, autoCancel: false },
  );

  useEffect(() => {
    async function getUserStatus() {
      try {
        const { data } = await fetch({ url: '/status' });
        setIsAuthenticated(data.isAuthenticated);
      } catch (error) {
        setError(error);
      }
    }

    getUserStatus();
  }, []);

  function login() {
    window.location.replace(import.meta.env.VITE_API_BASE_URL + '/login');
  }

  async function logout() {
    try {
      await fetch({ url: '/logout' });
      setIsAuthenticated(false);
    } catch (error) {
      setError(error);
    }
  }

  const value = useMemo(() => {
    return {
      error,
      isAuthenticated,
      login,
      logout,
    };
  }, [data, error, isAuthenticated]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

AuthProvider.propTypes = {
  children: PropTypes.any,
};
