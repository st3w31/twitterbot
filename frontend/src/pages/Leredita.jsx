import useAxios from 'axios-hooks';
import { endOfDay } from 'date-fns';
import { Button, Spinner, Table } from 'flowbite-react';
import { useState } from 'react';

import DatePicker from '@/components/DatePicker';
import Error from '@/components/Error';

export default function Leredita() {
  const [winners, setWinners] = useState();
  const [startTime, setStartTime] = useState();
  const [endTime, setEndTime] = useState();
  // const [word, setWord] = useState();

  /**
   * Handle date picker.
   *
   * @param {Date} startTime Start time.
   * @param {Date} endTime End time.
   */
  function handleDatePicker(startTime, endTime) {
    setStartTime(startTime);
    setEndTime(endTime ? endTime : endOfDay(startTime));
  }

  const [{ loading, error }, fetchWinners] = useAxios(
    {
      method: 'POST',
      baseURL: import.meta.env.VITE_API_BASE_URL,
      headers: { 'Content-Type': 'application/json' },
      responseType: 'json',
    },
    { manual: true },
  );

  async function getWinners() {
    const dateFilter = {
      startTime: startTime.toISOString(),
      endTime: endTime.toISOString(),
    };
    const res = await fetchWinners({
      url: '/eredita',
      data: {
        ...dateFilter,
      },
    });
    setWinners(res.data.data);
    // setWord(res.data.word);
  }

  function handleClick(event) {
    event.preventDefault();
    getWinners();
  }

  return (
    <div className="container mx-auto px-4">
      <Error error={error} />
      <div className="inline-flex mb-3 mt-3">
        <div className="pl-1">
          <DatePicker
            handleDatePicker={handleDatePicker}
            customProp={
              'text-gray-900 bg-gray-100 border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700'
            }
          />
        </div>
        <div className="pr-1">
          <Button onClick={handleClick}>Search Winners</Button>
        </div>
      </div>

      {loading ? (
        <div className="mb-3 mt-3 px-10">
          <Spinner aria-label="Default status example" />
        </div>
      ) : (
        !error &&
        winners && (
          <div className="mb-3 mt-3">
            <h2 className=" px-10 text-4xl font-extrabold dark:text-white mb-3">
              Ecco i vincitori della #ghigliottina su twitter!
            </h2>
            <Table striped={true}>
              <Table.Head>
                <Table.HeadCell>Posizione</Table.HeadCell>
                <Table.HeadCell>Vincitori</Table.HeadCell>
                <Table.HeadCell>Orario del tweet</Table.HeadCell>
              </Table.Head>
              <Table.Body className="divide-y">
                {winners?.map((winner, index) => {
                  return (
                    <Table.Row
                      key={winner.id}
                      className="bg-white dark:border-gray-700 dark:bg-gray-800"
                    >
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {index + 1 + '°'}
                      </Table.Cell>
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {winner.user}
                      </Table.Cell>
                      <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                        {winner.time}
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
              </Table.Body>
            </Table>
          </div>
        )
      )}
    </div>
  );
}
