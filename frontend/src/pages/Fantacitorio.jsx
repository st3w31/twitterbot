import useAxios from 'axios-hooks';
import { endOfDay, subDays, subSeconds } from 'date-fns';
import { Button, Spinner } from 'flowbite-react';
import { useEffect, useState } from 'react';

import DatePicker from '@/components/DatePicker';
import Content from '@/components/fantacitorio/Content';

function Fantacitorio() {
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [fantaPoints, setFantaPoints] = useState(null);

  const fantacitorioUsername = 'Fanta_citorio';
  const maxResults = 200;

  useEffect(() => {
    const [startTime, endTime] = defaultDateFilter();
    setStartTime(startTime);
    setEndTime(endTime);
  }, []);

  /**
   * Handle date picker.
   *
   * @param {Date} startTime Start time.
   * @param {Date} endTime End time.
   */
  function handleDatePicker(startTime, endTime) {
    setStartTime(startTime);
    setEndTime(endTime ? endTime : endOfDay(startTime));
  }

  /**
   * Default dates.
   *
   * @returns {[Date, Date]} Default dates.
   */
  function defaultDateFilter() {
    const endTime = subSeconds(new Date(), 10);
    const startTime = subDays(endTime, '7');
    return [startTime, endTime];
  }

  // TOGLIERE DOPPIO USEAXIOS
  const [{ loading }, fetchTweets] = useAxios(
    {
      method: 'POST',
      baseURL: import.meta.env.VITE_API_BASE_URL,
      headers: { 'Content-Type': 'application/json' },
      responseType: 'json',
    },
    { manual: true },
  );

  async function fetchFantacitorio() {
    const dateFilter = {
      startTime: startTime.toISOString(),
      endTime: endTime.toISOString(),
    };

    fetchTweets({
      url: '/tweets/by/username',
      data: {
        username: fantacitorioUsername,
        ...dateFilter,
        maxResults,
      },
    }).then((response) => {
      setFantaPoints(response);
    });
  }

  function handleStartButton(event) {
    event.preventDefault();
    fetchFantacitorio();
  }

  return (
    <div className="container mx-auto py-10 px-3">
      {!fantaPoints ? (
        <div className="flex flex-row justify-center items-center">
          {loading ? (
            <div className="flex h-full items-center justify-center">
              <Spinner size="xl" aria-label="Loading tweets..." />
            </div>
          ) : (
            <div className="flex flex-col items-center">
              <div className="pl-1">
                <DatePicker
                  handleDatePicker={handleDatePicker}
                  customProp={
                    'text-gray-900 bg-gray-100 border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-full text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700'
                  }
                />
              </div>
              <Button
                onClick={handleStartButton}
                className="shadow-md"
                gradientDuoTone="cyanToBlue"
              >
                Start Fantacitorio
              </Button>
            </div>
          )}
        </div>
      ) : (
        <Content
          points={fantaPoints?.data}
          startTime={startTime}
          endTime={endTime}
        />
      )}
    </div>
  );
}

export default Fantacitorio;
