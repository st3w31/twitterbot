import useAxios from 'axios-hooks';
import { addSeconds } from 'date-fns';
import { Button } from 'flowbite-react';
import { Position } from 'kokopu';
import { Chessboard } from 'kokopu-react';
import { isEqual } from 'lodash-es';
import { useEffect, useReducer } from 'react';
import { useTimer } from 'react-timer-hook';

const pieces = Object.freeze({
  white: { color: 'w', label: 'White' },
  black: { color: 'b', label: 'Black' },
});

const gameState = Object.freeze({
  /** Game still open. */
  OPEN: 'OPEN',
  /** Game ended in a draw. */
  DRAW: 'DRAW',
  /** White won. */
  WINW: 'WHITE WON',
  /** Black won. */
  WINB: 'BLACK WON',
});

const initialState = {
  board: new Position(),
  me: pieces.white,
  opponent: pieces.black,
  turn: pieces.white.color,
  gameState: gameState.OPEN,
  flipped: false,
  pollId: null,
  opponentMoves: [],
  expiryTimestamp: () => addSeconds(new Date(), 30),
};

function reducer(state, action) {
  switch (action.type) {
    case 'CREATE_POLL': {
      const { pollId, opponentMoves } = action.payload;

      return { ...state, pollId, opponentMoves };
    }

    case 'OPPONENT_MOVE': {
      const { board, me } = state;
      const { move } = action.payload;

      board.play(move);
      board.turn(me.color);

      return {
        ...state,
        turn: me.color,
        pollId: initialState.pollId,
        opponentMoves: initialState.opponentMoves,
      };
    }

    case 'MY_MOVE': {
      const { board, opponent } = state;
      const { move } = action.payload;

      board.play(move);
      board.turn(opponent.color);

      return { ...state, turn: opponent.color };
    }

    case 'GAME_STATE': {
      const { gameState } = action.payload;

      return { ...state, gameState };
    }

    case 'FLIP_SIDES': {
      const { me, opponent, flipped } = state;

      const flip = (side) => {
        if (isEqual(side, pieces.white)) {
          return pieces.black;
        }

        if (isEqual(side, pieces.black)) {
          return pieces.white;
        }
      };

      return {
        ...initialState,
        me: flip(me),
        opponent: flip(opponent),
        flipped: !flipped,
      };
    }

    case 'RESET': {
      const { board } = state;

      board.reset();

      return {
        ...initialState,
        board,
      };
    }

    default:
      throw new Error(`Unknown action type: ${action.type}`);
  }
}

function Chess() {
  const [
    {
      board,
      me,
      opponent,
      turn,
      gameState,
      flipped,
      pollId,
      opponentMoves,
      expiryTimestamp,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const { minutes, seconds, isRunning, start, restart } = useTimer({
    expiryTimestamp: expiryTimestamp(),
    autoStart: false,
  });

  // eslint-disable-next-line no-unused-vars
  const [_, chess] = useAxios( //NOSONAR
    {
      method: 'POST',
      baseURL: import.meta.env.VITE_API_BASE_URL,
      headers: { 'Content-Type': 'application/json' },
      responseType: 'json',
      withCredentials: true,
    },
    { manual: true },
  );

  // Every time the board changes check if the game is over.
  useEffect(() => {
    if (board.isDead()) {
      onGameDead();
    }

    if (board.isCheckmate()) {
      onCheckmate();
    }
  }, [board]);

  async function createPoll() {
    try {
      const fen = board.fen();
      const moves = board.moves().map((move) => board.notation(move));

      const { data } = await chess({
        url: '/chess/poll/create',
        data: { moves: moves.toString(), fen: fen.toString() },
      });

      dispatch({
        type: 'CREATE_POLL',
        payload: { pollId: data.id, opponentMoves: moves },
      });

      start();
    } catch (error) {
      console.log(error);
    }
  }

  async function handleOpponentMove() {
    try {
      const { data } = await chess({
        url: '/chess/poll/replies',
        data: { pollId },
      });

      dispatch({
        type: 'OPPONENT_MOVE',
        payload: {
          move:
            opponentMoves.indexOf(data.move) !== -1
              ? data.move
              : opponentMoves[0],
        },
      });

      restart(expiryTimestamp(), false);
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Handle my move.
   *
   * @param {string} move Move.
   */
  function handleMyMove(move) {
    if (isMyTurn()) {
      dispatch({ type: 'MY_MOVE', payload: { move } });
    }
  }

  /**
   * Both players have insufficient material so the game cannot end in checkmate.
   */
  const onGameDead = () =>
    dispatch({
      type: 'GAME_STATE',
      payload: { gameState: gameState.DRAW },
    });

  /**
   * The player that is about to play is checkmated.
   */
  const onCheckmate = () => {
    if (turn === pieces.white.color) {
      dispatch({
        type: 'GAME_STATE',
        payload: { gameState: gameState.WINB },
      });
    }

    if (turn === pieces.black.color) {
      dispatch({
        type: 'GAME_STATE',
        payload: { gameState: gameState.WINW },
      });
    }
  };

  const isMyTurn = () => board.turn() === me.color;
  const isOpponentTurn = () => board.turn() === opponent.color;
  const reset = () => dispatch({ type: 'RESET' });

  /**
   * The white player becomes the black player and vice versa.
   */
  const flipSides = () => {
    dispatch({ type: 'FLIP_SIDES' });
    restart(expiryTimestamp(), false);
  };

  const renderTimer = () => {
    const padStart = (value) => String(value).padStart(2, '0');

    return (
      <>
        {padStart(minutes)}:{padStart(seconds)}
      </>
    );
  };

  return (
    <div className="flex flex-row justify-center">
      {gameState === 'OPEN' ? (
        <>
          <Chessboard
            position={board}
            animated={true}
            colorset="scid"
            squareSize={70}
            interactionMode="playMoves"
            onMovePlayed={(move) => handleMyMove(move)}
            flipped={flipped}
          />

          <div className="flex flex-col space-y-5">
            {isOpponentTurn() && (
              <div>
                {!pollId && (
                  <Button
                    size="sm"
                    color="success"
                    fullSized={true}
                    onClick={createPoll}
                  >
                    Create poll
                  </Button>
                )}

                {isRunning && (
                  <div>
                    <h1 className="font-medium text-lg">
                      Poll expires in: {renderTimer()}
                    </h1>
                  </div>
                )}

                {pollId && !isRunning && (
                  <Button
                    size="sm"
                    color="success"
                    fullSized={true}
                    onClick={handleOpponentMove}
                  >
                    Handle Opponent Move
                  </Button>
                )}
              </div>
            )}

            <div>
              <h1 className="font-medium text-lg mb-2">Match actions:</h1>

              <div className="flex flex-row space-x-2.5">
                <Button size="sm" color="info" onClick={flipSides}>
                  Flip sides
                </Button>

                <Button size="sm" color="failure" onClick={reset}>
                  Reset
                </Button>
              </div>
            </div>

            <div>
              <h1 className="font-medium text-lg mb-2">Match info:</h1>

              <ul className="list-disc list-inside">
                <li>You: {me.label}</li>
                <li>Opponent: {opponent.label}</li>
              </ul>
            </div>
          </div>
        </>
      ) : (
        <div className="flex flex-col space-y-2.5 items-center">
          <p className="font-medium text-lg">{gameState}!</p>

          <Button size="sm" color="info" onClick={reset}>
            Restart
          </Button>
        </div>
      )}
    </div>
  );
}

export default Chess;
