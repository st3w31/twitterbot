import useAxios from 'axios-hooks';
import { endOfDay } from 'date-fns';
import { Spinner } from 'flowbite-react';
import { useState } from 'react';

import Chart from '@/components/Chart';
import Error from '@/components/Error';
import Pagination from '@/components/Pagination';
import Sentiment from '@/components/Sentiment';
import Slider from '@/components/Slider';
import SortFilters from '@/components/SortFilters';
import TermCloud from '@/components/TermCloud';
import Tweets from '@/components/Tweets';
import Map from '@/components/map/Map';
import SearchBar from '@/components/searchbar/SearchBar';

export default function Home() {
  const [type, setType] = useState('');
  const [query, setQuery] = useState('');
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [maxResults, setMaxResults] = useState(10);
  const [sortType, setSortType] = useState('');

  /**
   * Handle range input.
   *
   * @param {Event} event Event.
   */
  function handleRangeInput(event) {
    setMaxResults(Number(event.target.value));
  }

  /**
   * Handle form submit.
   *
   * @param {Event} event Event.
   */
  function handleFormSubmit(event) {
    event.preventDefault();
    getTweets();
  }

  /**
   * Handle search input.
   *
   * @param {Event} event Event.
   */
  function handleSearchInput(event) {
    setQuery(event.target.value);
  }

  /**
   * Handle search type.
   *
   * @param {string} type Type.
   */
  function handleSearchType(type) {
    setType(type);
  }

  /**
   * Handle date picker.
   *
   * @param {Date} startTime Start time.
   * @param {Date} endTime End time.
   */
  function handleDatePicker(startTime, endTime) {
    setStartTime(startTime);
    setEndTime(endTime ? endTime : endOfDay(startTime));
  }

  /**
   * Handle sort type.
   *
   * @param {string} type Sort type.
   */
  function handleSortType(type) {
    setSortType(type);
  }

  const [{ data, loading, error }, fetchTweets] = useAxios(
    {
      method: 'POST',
      baseURL: import.meta.env.VITE_API_BASE_URL,
      headers: { 'Content-Type': 'application/json' },
      responseType: 'json',
    },
    { manual: true },
  );

  function getTweets() {
    const dateFilter = {
      startTime: startTime.toISOString(),
      endTime: endTime.toISOString(),
    };

    if (type === 'username') {
      return fetchTweets({
        url: '/tweets/by/username',
        data: {
          username: query,
          ...dateFilter,
          maxResults,
        },
      });
    }

    if (type === 'location') {
      const countryCodes = query
        .split(',')
        .map((value) => value.replaceAll(' ', ''));

      return fetchTweets({
        url: '/tweets/by/location',
        data: {
          countryCodes,
          ...dateFilter,
          maxResults,
        },
      });
    }

    if (type === 'hashtag') {
      const keywords = query
        .split(',')
        .map((value) => value.replaceAll(' ', ''));

      return fetchTweets({
        url: '/tweets/by/hashtag',
        data: {
          keywords,
          ...dateFilter,
          maxResults,
        },
      });
    }
  }

  return (
    <div className="flex flex-col gap-y-5">
      <Error error={error} />

      <SearchBar
        handleFormSubmit={handleFormSubmit}
        handleSearchInput={handleSearchInput}
        handleSearchType={handleSearchType}
        handleDatePicker={handleDatePicker}
      />

      <SortFilters handleSortType={handleSortType} />

      <div className="flex flex-col items-center justify-center">
        <Slider handleRangeInput={handleRangeInput} maxResults={maxResults} />
      </div>

      {loading ? (
        <div className="flex h-full items-center justify-center">
          <Spinner size="xl" aria-label="Loading tweets..." />
        </div>
      ) : (
        data?.tweets && (
          <Pagination items={data?.tweets} itemsPerPage={10} itemsAs="tweets">
            <Tweets
              users={data?.users}
              media={data?.media}
              sortType={sortType}
              tweetCustomProps={{
                container:
                  'rounded-md bg-white p-4 shadow-[0px_0px_5px_0px] shadow-blue-500/50 transition ease-in-out hover:scale-[102.5%]',
                showTwitterLogo: true,
              }}
              containerClassName="grid grid-flow-row auto-rows-auto grid-cols-1 place-content-center place-items-center gap-3.5 md:grid-cols-2"
            />
          </Pagination>
        )
      )}

      <div className="h-[30rem]">
        <Map
          tweets={data?.tweets}
          users={data?.users}
          places={data?.places}
          media={data?.media}
        />
      </div>

      {data?.tweets && (
        <div className="flex flex-row space-x-5 justify-center">
          {type === 'username' && (
            <div className="bg-white h-96 border rounded-md shadow px-2 py-4 w-1/2">
              <Chart tweets={data.tweets} />
            </div>
          )}

          <div className="bg-white h-96 border rounded-md shadow px-2 py-4 w-1/2">
            <Sentiment tweets={data.tweets} />
          </div>

          {
            <div className="bg-white rounded-md shadow border h-96">
              <TermCloud tweets={data?.tweets} />
            </div>
          }
        </div>
      )}
    </div>
  );
}
