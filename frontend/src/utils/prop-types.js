import PropTypes from 'prop-types';

export function tweetPropTypes({
  id,
  text,
  authorId,
  createdAt,
  publicMetrics,
  geo,
} = {}) {
  const props = {
    id: id ?? true,
    text: text ?? true,
    authorId: authorId ?? true,
    createdAt: createdAt ?? true,
    publicMetrics: publicMetrics ?? true,
    geo: geo ?? true,
  };

  return PropTypes.shape({
    ...(props.id && { id: PropTypes.string.isRequired }),
    ...(props.text && { text: PropTypes.string.isRequired }),
    ...(props.authorId && { author_id: PropTypes.string.isRequired }),
    ...(props.createdAt && { created_at: PropTypes.string.isRequired }),
    ...(props.publicMetrics && {
      public_metrics: PropTypes.shape({
        like_count: PropTypes.number.isRequired,
        reply_count: PropTypes.number.isRequired,
        retweet_count: PropTypes.number.isRequired,
      }),
    }),
    ...(
      props.geo && {
        geo: {
          place_id: PropTypes.string.isRequired,
        },
      }
    ).isRequired,
  });
}

export function mediaPropTypes({ url, width, type, height } = {}) {
  const props = {
    url: url ?? true,
    width: width ?? true,
    height: height ?? true,
    type: type ?? true,
  };

  return PropTypes.shape({
    ...(props.url && { url: PropTypes.string.isRequired }),
    ...(props.width && { width: PropTypes.number }),
    ...(props.height && { height: PropTypes.number }),
    ...(props.type && { type: PropTypes.string }),
  }).isRequired;
}

export function userPropTypes({
  name,
  username,
  profileImageUrl,
  _protected,
  verified,
} = {}) {
  const props = {
    name: name ?? true,
    username: username ?? true,
    profileImageUrl: profileImageUrl ?? true,
    _protected: _protected ?? true,
    verified: verified ?? true,
  };

  return PropTypes.shape({
    ...(props.name && { name: PropTypes.string.isRequired }),
    ...(props.username && { username: PropTypes.string.isRequired }),
    ...(props.profileImageUrl && {
      profile_image_url: PropTypes.string.isRequired,
    }),
    ...(props._protected && { protected: PropTypes.bool.isRequired }),
    ...(props.verified && { verified: PropTypes.bool.isRequired }),
  }).isRequired;
}

export function placePropTypes({ id, geo } = {}) {
  const props = {
    id: id ?? true,
    geo: geo ?? true,
  };

  return PropTypes.shape({
    ...(props.id && { id: PropTypes.string.isRequired }),
    ...(props.geo && {
      geo: PropTypes.shape({
        bbox: PropTypes.arrayOf(PropTypes.number).isRequired,
      }),
    }),
  }).isRequired;
}
