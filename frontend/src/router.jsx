import { Navigate, createBrowserRouter } from 'react-router-dom';

import Chess from './pages/Chess';
import Fantacitorio from './pages/Fantacitorio';
import Home from './pages/Home';
import Leredita from './pages/Leredita';
import Base from './shared/Base';

export default createBrowserRouter([
  {
    path: '/',
    element: <Base />,
    children: [
      {
        index: true,
        element: <Home />,
      },
      {
        path: '/eredita',
        element: <Leredita />,
      },
      {
        path: '/fantacitorio',
        element: <Fantacitorio />,
      },
      {
        path: '/chess',
        element: <Chess />,
      },
    ],
  },
  {
    path: '*',
    element: <Navigate to="/" />,
  },
]);
