import { Fragment } from 'react';
import { Outlet } from 'react-router-dom';

import Footer from '@/components/Footer';
import Header from '@/components/header/Header';

export default function Base() {
  return (
    <Fragment>
      <Header />

      <main className="flex-grow bg-slate-50">
        <div className="container mx-auto py-10 px-3">
          <Outlet />
        </div>
      </main>

      <Footer />
    </Fragment>
  );
}
