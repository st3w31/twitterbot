/** @type {import('eslint').ESLint.ConfigData} */
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    ecmaVersion: 2022,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime',
    'plugin:import/recommended',
    'plugin:jsx-a11y/recommended',
    /**
     * This disables the formatting rules in ESLint that Prettier is going to
     * be responsible for handling. Make sure it's always the last config,
     * so it gets the chance to override other configs.
     */
    'plugin:prettier/recommended',
  ],
  settings: {
    react: {
      // Tells eslint-plugin-react to automatically detect the version of React to use.
      version: 'detect',
    },
    'import/resolver': {
      node: {
        paths: ['src'],
        extensions: ['.js', '.jsx'],
      },
      alias: {
        map: [['@', './src/']],
        extensions: ['.js', '.jsx'],
      },
    },
  },
  rules: {
    'import/no-unresolved': 0,
  },
};
