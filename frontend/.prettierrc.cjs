/** @type {import('prettier').Config} */
module.exports = {
  plugins: [
    require('prettier-plugin-tailwindcss'),
    require('@trivago/prettier-plugin-sort-imports'),
  ],

  // prettier
  singleQuote: true,
  trailingComma: 'all',

  // @trivago/prettier-plugin-sort-imports
  importOrder: ['^@/(.*)$', '^[./]'],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,
  importOrderGroupNamespaceSpecifiers: true,
};
